function qmMultiModule () {
    var self = this;
    this.qmWmid = '';
    this.qmSid = '';
    this.qmEnv = 'app';
    this.qmVersion = '';
    this.qmSymbolParam = '';
    this.qmSymbol = '';
    this.qmLang = '';
    this.qmActiveNavId = "qmNav0";
    this.qmToolsArray = [];
  
    this.qmBuildNav = function () {
      for (qmIdx = 0; qmIdx < this.qmToolsArray.length; qmIdx++) {
        var span = document.createElement('span');
        span.id = 'qmNav' + qmIdx;
        span.setAttribute('class', 'qmTab' );
        span.innerHTML = this.qmToolsArray[qmIdx].displayname;
        document.getElementById('qmNavigation').appendChild(span);
      }
      var qmElem = document.getElementById(this.qmActiveNavId);
      qmElem.setAttribute('class', 'qmTab qmActiveTab');
    };
  
    this.qmStyleActiveTab = function () {
      for (qmIdx = 0; qmIdx < this.qmToolsArray.length; qmIdx++) {
        var qmId = 'qmNav' + qmIdx;
        var qmElem = document.getElementById(qmId);
        if (qmId == this.qmActiveNavId) {
          qmElem.setAttribute('class', 'qmTab qmActiveTab');
        } else {
          qmElem.setAttribute('class', 'qmTab');
        }
      }
    };
  
    this.qmShowTool = function () {
      var qmId = parseInt(this.qmActiveNavId.replace('qmNav',''), 10);
      var qmParams = this.qmToolsArray[qmId].params
      var qmodLoader = document.getElementById('qmodLoader');
      var qmodTool = document.getElementById('qmodTool');
      var script = document.createElement('script');
  
      qmodTool.setAttribute('data-qmod-tool', this.qmToolsArray[qmId].toolname);
      qmodTool.setAttribute('data-qmod-params', qmParams);
  
      script.type= 'text/javascript';
      script.id = 'qmod';
      script.setAttribute('data-qmod-wmid', this.qmWmid);
      script.setAttribute('data-qmod-sid', this.qmSid);
      script.setAttribute('data-qmod-version', this.qmVersion);
      script.setAttribute('data-qmod-env', this.qmEnv);
      script.src= '//qmod.quotemedia.com/js/qmodLoader.js';
  
      qmodLoader.innerHTML = '';
      qmodLoader.appendChild(script);
    };
  
    this.qmGetParam = function (paramName) {
      var qmQuery = window.location.search.substring(1);
      var qmVars = qmQuery.split("&");
      var qmReturn = "";
      for (var i=0; i < qmVars.length; i++) {
        var qmPair = qmVars[i].split("=");
        if (qmPair[0] == paramName) {
          qmReturn = qmPair[1];
        }
      }
      return(qmReturn);
    };
  
    this.qmToggleClass = function (qmElem, qmClassName) {
      if (qmElem.classList) {
        qmElem.classList.toggle(qmClassName);
      } else {
        var classes = qmElem.classList.split(' ');
        var existingIndex = classes.indexOf(qmClassName);
        if (existingIndex >= 0) {
          classes.splice(existingIndex, 1);
        } else {
          classes.push(qmClassName);
        }
        qmElem.classList = classes.join(' ');
      }
    };
  
    this.qmDocReady = function() {
      console.log("qmDocReady START");
      self.qmBuildNav();
      var qmNavItems = document.querySelectorAll('span.qmTab');
      for (var qmItem = 0; qmItem < qmNavItems.length; qmItem++) {
        var item = qmNavItems[qmItem];
        item.onclick = function(e) {
          e.preventDefault();
          self.qmActiveNavId = e.target.id;
          self.qmShowTool();
          self.qmStyleActiveTab();
        };
      }
      self.qmShowTool();
    };
  
    this.init = function () {
      this.qmSymbolParam = decodeURIComponent(this.qmGetParam("symbol"));
  
      if (this.qmSymbolParam != "") {
        this.qmSymbol = this.qmSymbolParam;
      }
    };
  
    this.run = function () {
      if (this.qmToolsArray.length == 0) {console.log("Tools not configured");return;}
  
      // Do not use queryString for symbol
      //this.init();
  
      if (document.readyState != 'loading'){
        this.qmDocReady();
      } else {
        document.addEventListener('DOMContentLoaded', this.qmDocReady);
      }
    };
  
    this.qmAddTool = function (toolname, displayname, params) { this.qmToolsArray.push({toolname:toolname, displayname:displayname, params:params }); };
    this.qmGetWmid = function () { return this.qmWmid; };
    this.qmSetWmid = function (wmid) { this.qmWmid = wmid; };
    this.qmGetVersion = function () { return this.qmVersion; };
    this.qmSetVersion = function (version) { this.qmVersion = version; };
    this.qmGetSid = function () { return this.qmSid; };
    this.qmSetSid = function (sid) { this.qmSid = sid; };
    this.qmGetEnv = function () { return this.qmEnv; };
    this.qmSetEnv = function (env) { this.qmEnv = env; };
    this.qmGetSymbol = function () { return this.qmSymbol; };
    this.qmSetSymbol = function (symbol) { this.qmSymbol = symbol; };
    this.qmGetLang = function () { return this.qmLang; };
    this.qmSetLang = function (lang) { this.qmLang = lang; };
  
  }
  