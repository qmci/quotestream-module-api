// Get styles from page and push to charts
var timerLoopCount = 0;

var chartAnimation = {
  in: "fadeIn",
  out: "fadeOut"
};

var chartColors = {
  dark: {
    chbg: '000000', // transparent
    chbgch: '000000', // transparent
    chgrdon: 'off',
    chgrd: '474747',
    chfill: '000000', // 7cb5ec
    chfill2: '295e91',
    chln: 'cccccc',
    chbdr: '000000',
    chfnts: 10,
    chxyc: 'FFFFFF',
    chvolon: 'on',
    chlgdon: 'off',
    chtcol: 'FF0000',
    chcon: 'off',
    ch_fill_01: 'dedede',
    ch_fill_02: 'b4b4b4',
    ch_fill_03: '949494',
    ch_fill_04: '7b7b7b',
    ch_fill_05: '515151'
  }
};

var styleInterval,actionInterval,chartCheckInterval;
var bound = false;
var loading = false;
var watching = false;
var timerSet = false;

function getPageStyles() {
  // get colors, etc. from document styles
  var styles = {
    font: $('body').css("font-family"),
    fontSize: $('body').css("font-size"),
    primary: $('.qmod-ui-tool .qmod-heading').css("color"),
    background: $('body').css("background-color")
  };
  return styles;
}

function rgbToHex(r, g, b) {
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function hexToRgb(hex) {
  var int = parseInt(hex, 16);
  var r = (int >> 16) & 255;
  var g = (int >> 8) & 255;
  var b = int & 255;
  return r + "," + g + "," + b;
}

var buttonsAdded = false;

function addActions() {
  if (bound === false) {
    var legend = $('.qmod-legend, .qmod-button-group');
    var legendItems = $(".qmod-legend li, .qmod-button-group li");
    var buttons = $('.qmod-btn:not([rv-on-click="data.popup.close"]), .highcharts-legend-item, .qmod-popup-link, [rv-on-click="data.clickRow"]');
    var added = false;
    var tabs = $('.qmod-tab-item-link, .qmod-control-btn, .qmod-dropdown a');

    if (tabs.length > 0) {
      $(tabs).click(function(e) {
        window.setTimeout(function() {
          buttonsAdded = false;
          addActions();
        }, 500);
      });
    }

    if (legend.length > 0 && legendItems.length > 0) {
      legendItems.each(function() {
        $(this).on("click", function() {
          event.preventDefault();
          flipChart();

          window.setTimeout(function() {
            flipChart();
          }, 400);

          added = true;
        });
      });
      bound = true;
    }

    if (buttonsAdded === false && $(buttons).length > 0) {
      $(buttons).click(function(e) {
        e.preventDefault();
        addStyles(true);
      });
      buttonsAdded = true;
    }

    if (added === true) clearInterval(actionInterval);
  }
}

function matchParams(params, pageStyles, joiner) {
  joiner = (typeof joiner === 'undefined') ? "," : joiner;
  
  var match = false;
  var paramArr = params.split("&");
  var paramsArr = [];

  paramArr.forEach(function(p) {
    match = false;
    
    if (p.indexOf("=") !== -1) {
      var pSplit = p.split("=");
      
      if (pageStyles[pSplit[0]] !== undefined) {
        Object.entries(pageStyles).forEach((s) => {
          paramsArr.push(s[0] + "=" + s[1]);
          match = true;
        });
      } else {
        var colorMatches = 0;
        for (var theme in chartColors) {
          for (var name in chartColors[theme]) {
            var val = chartColors[theme][name];
            
            if (pSplit[0] === name) {
              paramsArr.push(name + "=" + val);
              match = true;
            }
          }
        }
      }
    }
    if (match === false) {
      paramsArr.push(p);
    }
  });
  if (joiner === "&") {
    var string = "";
    paramsArr.forEach(function(p) {
      if (string !== "") string += "&";
      string += p;
    });
    return string;
  }
  return paramsArr;
}

function addParams(paramsArr) {
  var newSrcParams = "";
  for (var x = 0; x < paramsArr.length; x++) {
    if (x > 0) newSrcParams += "&";
    newSrcParams += paramsArr[x];
  }
  return newSrcParams;
}

function flipChart(dir) {
  var chartClass = "qmod-tab-img";
  if (
    $(".qmod-topChart-img").length > 0 &&
    $(".qmod-ui-modal").length === 0
  ) {
    chartClass = "qmod-topChart-img";
  }

  if ($('.highcharts-container').length > 0) {
    chartClass = "highcharts-container";
    setHighchartsStyles(
      getPageStyles()
    );
    return 0;
  }

  if (loading === false && (dir === null || typeof dir === 'undefined')) {
    window.setTimeout(function() {
      $("." + chartClass)
        .css('opacity', '1')
        .removeClass()
        .addClass(chartClass +
          ' ' +
          chartAnimation.in +
          ' animated'
      );
    }, 1600);
  } else if (dir === 'out') {
    loading = true;

    window.setTimeout(function() {
      $("." + chartClass)
        .removeClass()
        .addClass(chartClass +
          ' ' +
          chartAnimation.out +
          ' animated'
      );
      loading = false;
    }, 10);
  }
}

function addChartFader() {
  if ($('.qmod-chart-plot .chart_fader').length === 0) {
    $('.qmod-chart-plot').each(function(index, plot) {
      if ($(plot).find(".qmod-tab-img").attr("src") !== undefined) {
        $(plot).append('<div class="chart_fader"></div>');
      }
    });
  }
}

function updateChartSrc(chart, newSrc) {
  $(chart).prop("src", newSrc);
  $('.qmod-chart-plot .chart_fader')
    .animate({opacity:'0'}, 800, function() {
      flipChart();
      loading = false;
  });
}

function watchChart(chart) {
  // watch for changes to chart src - update styles when necessary
  if (watching === false) {
    $(chart).on('load', function () {
      if (loading === false) {
        setQToolStyles();
      }
    });
    watching = true;
  }
}

function addStyles(click) {

  if ($('.highcharts-container').length > 0) {
    clearInterval(styleInterval);
    styleInterval = null;

    window.setTimeout(function() {
      setHighchartsStyles(
        getPageStyles()
      );
    }, 1);

    $(window).resize(function() {
      if (styleInterval === null) {
        runTimer('styles');
      }
    });
  } else if (
    $('.qmod-popup').length > 0 &&
    click === true
  ) {
    if ($('.qmod-popup .qmod-popup-body img').length > 0) {
      var src = $('.qmod-popup .qmod-popup-body img').attr('src').split("?");
      src[1] += '&chbg=000000&chbgch=000000&chxyc=FFFFFF';
      var newSrc = src[0] + "?" + matchParams(src[1], getPageStyles(), "&");

      $('.qmod-popup .qmod-popup-body img').attr('src', newSrc);
    }
  
  } else if ($('.qtool img.qmod-tab-img').length > 0) {
    if (click === true && loading !== true) {
      loading = true;

      // duplicate image
      $('.qtool img.qmod-tab-img')
        .clone()
        .appendTo('.qmod-chart-plot')
        .remove();

      window.setTimeout(function() {
        $('.qtool img.qmod-tab-img').removeClass().addClass('qmod-tab-img ' + chartAnimation.in +' animated').fadeIn(500, function() {
          loading = false;
        });
      }, 300);
    } else {
      clearInterval(styleInterval);
      $('.qmod-tab-img').removeClass().addClass('qmod-tab-img ' + chartAnimation.in +' animated').animate({opacity: 1}, "slow");
      setQToolStyles();
    }
  }
}

function setHighchartsStyles(chartStyles) {
  if ($('.highcharts-root').lentgth > 0 && chartCheckInterval === null) {
    runTimer('highcharts');
  }

  $('.highcharts-root').each(function() {
    $(this).find('.highcharts-root').css("font-family", chartStyles.font);
    $(this).find('.highcharts-background').attr("fill", chartStyles.background);
    $(this).find('text').css("font-size", chartStyles.fontSize);
    $(this).find('text').css("color", chartStyles.primary);
    $(this).find('text').css("fill", chartStyles.primary);
  });
  $('[rv-on-click="data.popChart"]').each(function() {
    $(this).click(function(e) {
      $('.qmod-modal-overlay .qmod-highchart').each (function() {
        $(this).find('.highcharts-root').css("font-family", chartStyles.font);
        $(this).find('.highcharts-background').attr("fill", chartStyles.background);
        $(this).find('text').css("font-size", chartStyles.fontSize);
        $(this).find('text').css("color", chartStyles.primary);
        $(this).find('text').css("fill", chartStyles.primary);
      });
    });
  });

  $('.highcharts-container').animate({opacity: 1}, "slow");
}

function fadeOut() {
  flipChart('out');
  window.setTimeout(function() {
    $('.qmod-chart-plot .chart_fader').animate({opacity:'1'}, 1100);
  }, 800);
}

function fadeIn() {
  $('.qmod-chart-plot .chart_fader')
    .animate({opacity:'1'}, 700, function() {
     flipChart();
     loading = false;
  });
}

function setQToolStyles() {
  if (
    $('.qtool .qmod-tab-img').length === 0
  ) {
    window.setTimeout(function() {
      if ($('.qmod-top-charts').length > 0) {
        $('.qmod-top-charts').each(function() {
          $(this).find('.qmod-topChart-img').each(function() {
            $(this).addClass(chartAnimation.in + ' animated');
          });
        });
     }
    }, 400);
    return 0;

  } else if (loading === false) {
    $('.qtool').each(function() {
      var height = $('.qmod-tab-img').css('height');

      if (height != "0px") {
        $('.qmod-chart-plot').height(height);
        $('.chart_fader').height(height);
      }

      $(this).find('.qmod-tab-img').each(function (index, chart) {
        var src = $(chart).prop("src");

        if (src !== "") {
          flipChart();
        }
      });
      clearInterval(styleInterval);
      styleInterval = null;
    });
  }
}

function runTimer(which) {
  if (which === 'styles') {
    styleInterval = window.setInterval(() => {
      timerLoopCount++;
      if (timerLoopCount < 10) {
        addStyles();

      } else {
        clearInterval(styleInterval);
        styleInterval = null;
      }
    }, 100);
  } else if (which === 'highcharts') {
    chartCheckInterval = window.setInterval(() => {
      var chartStyles = getPageStyles();
    }, 25);

  } else {
    actionInterval = window.setInterval(() => {
      timerLoopCount++;
      if (timerLoopCount < 10) {

        addActions();

      } else {
        clearInterval(actionInterval);
      }
    }, 500);
  }
}

function runTimers() {
  runTimer('styles');
  runTimer('actions');
  runTimer('highcharts');
}

function getUrlVars(url) {
  var vars = {};
  var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}

function setStyleLink() {
  const sid = $('#demo-sid').html().trim();

  if (sid.length > 10 && sid.indexOf("-") !== -1) {
    const currentUrl = window.location.toString();
    const demoUrlBase = currentUrl.split("&")[0];
    let newUrl = demoUrlBase + '&qmEnv=uat&sid=' + sid;

    if ($('#set-style-link').length === 0) {
      $('#dev_form .row.demo').append('<div class="col s2"><span id="set-style-link"><a href="#" class="waves-effect btn">set style</a></span></div>');
    }

    $('#dev_form').find('input,select').each(function() {
      if ($(this).val() !== "") {
        newUrl += '&' + $(this).attr('id') + '=' + $(this).val();
      }
    });

    $('#set-style-link a').attr('href', newUrl);
  }

}

function addSidToURL(sid) {
  const url = $(location).attr("href");
  let env = 'app';
  const setWMID = $('#wmid').val();

  if (url.indexOf('qmEnv') != -1) {
    const vars = getUrlVars(url);

    if (
      vars.qmEnv === 'uat' ||
      vars.qmEnv === 'dev' ||
      vars.qmEnv === 'stage' ||
      vars.qmEnv === 'qa'
    ) {
      env = vars.qmEnv;
    }
  }

  if (sid.length > 10) {
    const demoUrlBase = url.split("&")[0];
    let newUrl = demoUrlBase + '&qmEnv='+env+'&sid=' + sid + '&wmid=' + setWMID;
    window.location = newUrl;
  }
}

function activateDevForm() {
  $('#dev_form').find('input,select').each(function() {
    $(this).on("change keyup", function(e) {
      if ($(this).val() !== "") {
        const setParam = $(this).attr('id');
        const setVal = $(this).val();
        let params = [];

        $('.qm-tool-wrap').find('ul > li a').each(function() {
          let url = $(this).attr('href');
          const urlBase = url.split("&")[0];

          if (url.indexOf(setParam) != -1) {
            let vars = getUrlVars(url);

            if (setVal !== vars[setParam]) {
              let newHref = '?';
              let count = 0;

              Object.keys(vars).forEach(function(key) {
                if (key !== setParam) {
                  if (count > 0) newHref += '&';
                  newHref += key + '=' + vars[key];
                  params[key] = vars[key];
                  count++;
                }
              });
              newHref += '&' + setParam + '=' + setVal;
              $(this).attr('href', newHref);
            }
          } else {
            $(this).attr('href', $(this).attr('href') + '&' + setParam + '=' + setVal);
          }
        });

        setStyleLink();
      }
    });
  });
}

const loader = '<div class="loader">Loading...</div>';

function modalActions(url) {
  $('#qsmodule-preview').attr('src', url).css('visibility', 'hidden');
  $('#modal-content').append(loader);

  window.setTimeout(function() {
    $('#qsmodule-preview').css('visibility', 'visible');
    
  }, 4000);
}

function showPreview(title, url) {  
  if ($('#demo-preview-container .frame-container').find('.loader').length == 0) {
    $('#demo-preview-container .frame-container').prepend(loader);
  }

  $('#demo-preview-container .frame-container')
    .find('.loader')
    .removeClass(chartAnimation.out + " animated")
    .show()
    .addClass(chartAnimation.in +' animated');
  
  $('#demo-preview-container .frame-container .title')
    .removeClass(chartAnimation.in +' animated')
    .addClass(chartAnimation.out + " animated");

  $('#qsmodule-preview')
    .attr('src', url)
    .removeClass(chartAnimation.in + " animated")
    .addClass(chartAnimation.out + " animated");
  
  const queryString = url.split("?demo")[0];

  window.setTimeout(function() {

    const link = '<h3>' +
      '<a href="' +
      queryString +
      '" target="_blank">' +
      title +
      '</a>' +
      '</h3>';

    if ($('#demo-preview-container .frame-container .title').length === 0) {
      $('#demo-preview-container .frame-container').prepend('<div class="title '+ chartAnimation.in +' animated"></div>');
    }

    $('#demo-preview-container .frame-container .title')
      .html(link)
      .removeClass(chartAnimation.out + " animated")
      .addClass(chartAnimation.in + " animated");
    
    window.setTimeout(function(){
      $('#demo-preview-container .frame-container')
        .find('.loader')
        .removeClass(chartAnimation.in + " animated")
        .addClass(chartAnimation.out + " animated")
        .hide();

      $('#qsmodule-preview')
        .attr('src', url)
        .removeClass("hidden " + chartAnimation.out + " animated")
        .addClass(chartAnimation.in + " animated");
    }, 1000);
    
  }, 2500);
}

function getSid() {
  const wmid = $('#wmid').val();
  const user = $('#username').val();
  const pass = $('#password').val();

  if (
    wmid !== '' &&
    wmid !== 'undefined' &&
    user !== '' &&
    user !== 'undefined' &&
    pass !== '' &&
    pass !== 'undefined'
  ) {
    const url = $(location).attr("href");
    let env = 'app.';

    if (url.indexOf('qmEnv') != -1) {
      const vars = getUrlVars(url);

      if (
        vars.qmEnv === 'uat' ||
        vars.qmEnv === 'dev' ||
        vars.qmEnv === 'stage' ||
        vars.qmEnv === 'qa'
      ) {
        env = vars.qmEnv+'.';
      }
    }

    const getSidUrl = 'https://'+env+'quotemedia.com/auth/g/authenticate/v0/'+wmid+'/'+user+'/'+pass+'/';

    $.get( getSidUrl, function( data ) {
      if (
        data.code.name === 'Ok' &&
        (
          data.status === 'Active' ||
          data.status === 'QA'
        )
      ) {
        const sid = data.sid;
        $('#sid-input').val(sid).focus();
      }
    });
  }
}

$( document ).ready(function() {

  $('#set-sid-btn').on("click", function() {
    const sid = $('#sid-input').val();
    addSidToURL(sid);
  });

  if (
    $('#wmid').val() !== '' &&
    $('#wmid').length === 1 &&
    $('#username').val() !== '' &&
    $('#password').val() !== ''
  ) {
    getSid();
  }

  $('#open-admin-links').on("click", function() {
    if ($('.row.admin').hasClass('hidden')) $('.row.admin').removeClass('hidden');
    else $('.row.admin').addClass('hidden');
  });

  $('#open-demo-params').on("click", function() {
    if ($('.row.demo').hasClass('hidden')) $('.row.demo').removeClass('hidden');
    else $('.row.demo').addClass('hidden');
  });

  $('#get-sid-btn').on("click", function() {
    getSid();
  });

  if ($('#dev_form').length === 1) {
    activateDevForm();
  }

  // Run setup after QMod has done its thing
  runTimers();

  if (loadedQMods.length > 0) {
    window.setTimeout(function() {
      window.dispatchEvent(new Event('resize'));
    }, 1000);

  } else {
    M.updateTextFields();
  }

  $('#qmod-dev-list a').parent().on("click", function(event) {
    event.preventDefault();
    const url = $(this).find('a').attr("href");
    let title = $(this).find('a').text();

    $('#qmod-dev-list ul>li').removeClass('active');
    $(this).addClass('active');

    showPreview(title, url);
  });

  $( window ).resize(function() {
    timerLoopCount = 0;
    window.setTimeout(function() {
      runTimers();
    }, 800);
  });
});
