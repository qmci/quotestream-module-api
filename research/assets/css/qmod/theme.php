<?php
session_start();

$_ENV['css_dev'] = false; // whether to use local css (true) or generated/cached (false)

if (isset($_REQUEST['debug'])) {
  ini_set('display_errors', '1');
  ini_set('display_startup_errors', '1');
  error_reporting(E_ALL);
}

require_once('../../../functions/json.php');
require_once('../../../functions/Google.Sheets.api.php');
require_once('../../../functions/authentication.php');
require_once('../../../functions/params.php');
require_once('../../../functions/tool_list.inc.php');
require_once('../../../functions/setupVars.php');

function generateCSS() {
  $theme = $_SESSION['theme'];
  $size = $_SESSION['user_params']['chartParams']['chfnts'].'';
  $weight = $_SESSION['user_params']['fontWeight'];

  $cache_filename = $_ENV['css_cache_dir'].$theme.'_'.$size.'_'.$weight.'.css';

  header("Content-Type: text/css");

  if ($_ENV['css_dev'] == true && strstr($_SERVER['HTTP_HOST'], "local") && is_file('theme.css')) {
    return '/* LOCAL CSS - DARK */'.getCache('theme.css');

  } elseif (is_file($cache_filename)) {
    // return cache
    return '/* '.$cache_filename.' */'.getCache($cache_filename);

  } else {

    die('Error: Cache file not found: '.$cache_filename);

  }
}

echo generateCSS();

?>
