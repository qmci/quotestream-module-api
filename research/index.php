<?php

if (isset($_REQUEST['debug'])) {
  ini_set('display_errors', '1');
  ini_set('display_startup_errors', '1');
  error_reporting(E_ALL);
}

session_start();

/* VARS:

  REQUIRED:
    wmid
    sid
    page

  Optional:
    symbol
    colorTheme
    appTheme
    country
    lang
    font
    qmVersion
    qmEnv
    qmLoader
*/
require_once('functions/json.php');
require_once('functions/Google.Sheets.api.php');
require_once('functions/authentication.php');
require_once('functions/params.php');
require_once('functions/writeTags.php');
require_once('functions/tool_list.inc.php');
require_once('functions/setupVars.php');

$json = new Services_JSON();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Quotestream - Research Module</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="//static.c1.quotemedia.com/qs/img/favicon.ico">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/qmod/theme.php">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/4.2.1/iframeResizer.contentWindow.min.js" integrity="sha256-pcBCSNXBj1IVeOk2N+lpQd0JBrC3E0u9BXNKG33l5ZA=" crossorigin="anonymous"></script>
    <script>let loadedQMods = [];</script>

    <?php if (isset($_REQUEST['demo'])) { ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <?php } ?>

  </head>

  <body>
    <div class="container">
      <div class="qm-tool-wrap <?= $_ENV['available_params']->fontSize ?> <?= $_ENV['available_params']->colorTheme ?>">
        <?php
        
        $tool = @$pageInfo[$page]['tool'];
        $tabs = false;

        if (isset($_REQUEST['demo'])) {
          writeMenu($pageInfo);
          
        } elseif (
          sessionTest(
            $_ENV['available_params']->wmid,
            $_ENV['available_params']->sid
          ) == false
        ) {
          $errorMsg = ($_ENV['available_params']->lang == 'en') ? 
            'Oops - something went wrong. This may be due to an issue with your user session. Please logout and login again.' :
            'Oups, un problème est survenu. Cela peut être dû à un problème avec votre session utilisateur. Veuillez vous déconnecter et vous reconnecter.';
          
          echo '<p>'.$errorMsg.'</p>';
        
        } elseif (testRequiredParams($required_params)) {
          if ($type == 'qtQuoteModule') {
            if ($page == 'calendars') {
              writeCalendarMenu($clientForwardURL, $wmid, $qmEnv,$pageInfo[$page], $targetURL);
            }
            writeQuoteModule($qmEnv, $pageInfo[$page], $wmid, $targetURL);

          } elseif ($type == 'qmodii') {
            writeQMod_II($qmEnv, $wmid, $sid, $page, $pageInfo[$page], $advChartParams);

          } elseif ($page != '') {
            
            if(is_array($page) || is_array($tool) || strstr($page, ",") || strstr($tool, ",")) {
              // multi-tool
              $qmodTabs = array();

              if (is_string($tool) && strstr($tool, ",")) $pages = explode(",", $tool);
              elseif (is_array($page)) $pages = $page;
              else $pages = (is_array($tool)) ? $tool : explode(",", trim($page));

              // echo '<p>Multi-Page: <pre>',print_r($pages),'</pre>Tool: '.$tool.'</p>';die;

              foreach ($pages as $p) {
                $params = getDefaultParams($p);
                $title = getDefaultTitle($p);

                if ($params != false) {
                  
                  $pageInfo = array(
                    'tool' => $p,
                    'title' => $title,
                    'type' => 'qmod',
                    'param' => $json->encode($params),
                    'researchType' => '',
                    'colorType' => (isset($pageInfo[$p]['colorType'])) ? $pageInfo[$p]['colorType'] : false
                  );

                  if (
                    (
                      !is_array($tool) &&
                      !strstr(@$tool, ",")
                    ) || isset($_REQUEST['list'])
                  ) {
                    writeQMod($qmEnv, $wmid, $sid, $p, $pageInfo);

                  } else {
                    $qmodTabs[] = array($p, $pageInfo);
                  }
                }
              }
              if (is_array($qmodTabs) && count($qmodTabs) > 0) {
                $tabs = true;
                writeQModTabs($qmEnv, $wmid, $sid, $qmodTabs);
              }
            } elseif (!is_array($page) && isset($pageInfo[$page]) && @$pageInfo[$page]['param'] != false) {
                writeQMod($qmEnv, $wmid, $sid, $page, $pageInfo[$page]);

            } else {
              
              $params = getDefaultParams($page);

              // echo '<p>Default Params:</pre><pre>',print_r($params),'</pre>';die;

              if ($params != false) {
                
                $pageInfo = array(
                  'tool' => $page,
                  'type' => 'qmod',
                  'param' => $json->encode($params),
                  'researchType' => '',
                  'colorType' => (isset($pageInfo[$page]['colorType'])) ? $pageInfo[$page]['colorType'] : false
                );

                writeQMod($qmEnv, $wmid, $sid, $page, $pageInfo);

              } else {
                writeQMod($qmEnv, $wmid, $sid, $page);
              }
            }
          }
        }
        ?>
      
      </div>
    </div>
    <?php
    if ($tabs == false) writeQModLoader($qmodLoader, $qmEnv, $wmid, $sid, $qmodVersion);
    ?>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="assets/js/chartHelper.js"></script>
  </body>
</html>
