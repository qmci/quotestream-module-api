<?php
require_once('functions/json.php');
require_once('functions/authentication.php');
require_once('functions/setupVars.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Quotestream - Research Module</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="//static.c1.quotemedia.com/qs/img/favicon.ico">
	</head>

	<body>
    <?= getSID('101894', 'qmoddemo1', '101894Pass'); ?>
  </body>
</html>
