<?php
/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
  putenv('GOOGLE_APPLICATION_CREDENTIALS='.$_ENV['google']['account']['Service Account Key']);
  $json = new Services_JSON();
  $client = new Google_Client();
  $client->setApplicationName("QMod");
  
  $client->setScopes([
    "https://www.googleapis.com/auth/userinfo.email",
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/drive.file",
    "https://www.googleapis.com/auth/spreadsheets",
    "https://www.googleapis.com/auth/gmail.send",
    "https://www.googleapis.com/auth/contacts",
    "https://www.googleapis.com/auth/calendar",
    "https://www.googleapis.com/auth/calendar.events"
    /*"https://www.googleapis.com/auth/cloud-platform", // firesbase, firerstore
    "https://www.googleapis.com/auth/presentations", // slides
    "https://www.googleapis.com/auth/tasks"*/
  ]);

  // Use service account to access API
  if ($credentials_file = checkServiceAccountCredentialsFile()) {
    // set the location manually
    $client->setAuthConfig($_ENV['google']['account']['Service Account Key']);
    // echo '<p>SET AUTH CONFIG</p>';
    // var_dump($client);
    return $client;
  
  } elseif (getenv('GOOGLE_APPLICATION_CREDENTIALS')) {
    // use the application default credentials
    $client->useApplicationDefaultCredentials();
    // echo '<p>USE DEFAULT CREDENTIALS</p>';
    // var_dump($client);
    return $client;

  } else {
    echo missingServiceAccountDetailsWarning();
    return false;
  }

  // If there is no previous token or it's expired.
  if ($client->isAccessTokenExpired()) {
    // Refresh the token if possible, else fetch a new one.
    if ($client->getRefreshToken()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        echo '<p>Open the following link in your browser:</p><p><a href="'.$authUrl.'">'.$authUrl.'</a></p>';

        define('STDIN',fopen("php://stdin","r"));
        $authCode = trim(fgets(STDIN));

        echo '<p>authCode::</p>';
        var_dump($authCode);
        echo '<p>GET::</p>';
        var_dump($_GET);
        if (isset($_GET['code'])) $authCode = $_GET['code'];

        if ($authCode != '') {
          echo '<p>auth: '.$authCode.'</p>';
          // Exchange authorization code for an access token.
          $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
          $client->setAccessToken($accessToken);
          // Check to see if there was an error.
          if (array_key_exists('error', $accessToken)) {
              throw new Exception(join(', ', $accessToken));
          }

          // Save the token to a file (if it is set)
          if ($client->getAccessToken() != null) {
            file_put_contents($tokenPath, $json->encode($client->getAccessToken()));
          } else {
            echo '<p>No Auth Token Written</p>';
            var_dump($client->getAccessToken());
          }
        } else {
          echo '<p>No AUth Code Set</p>';
        }
    }
  } else {
    echo '<p>Authorized</p>';
  }
  return $client;
}