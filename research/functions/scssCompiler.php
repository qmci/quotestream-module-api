<?php
session_start();

require_once('json.php');
require_once('Google.Sheets.api.php');
require_once('authentication.php');
require_once('params.php');
require_once('tool_list.inc.php');
require_once('setupVars.php');
require_once('scssFunctions.inc.php');

requirePHPVersion(7);

$fontSize = ''.$_SESSION['user_params']['chfnts'];
$fontWeight = $_SESSION['user_params']['fontWeight'];

if (isset($_REQUEST['cache_css'])) {
  
  echo '<p>Caching all CSS...</p>';

  cacheAllThemes();

} elseif (isset($_REQUEST['cache_all'])) {

  echo '<p>Clearing cache...</p>';

  if (!is_dir($_ENV['css_cache_dir'])) {
    $_ENV['css_cache_dir'] = '../assets/css/theme_cache/';

    if (!is_dir($_ENV['css_cache_dir'])) die('Cache not found.');
  }

  clearCache($_ENV['css_cache_dir']);
  
  echo '<p>Caching all themes...</p>';

  cacheAllThemes();

} elseif (isset($_REQUEST['clear_cache'])) {

  echo '<p>Clearing cache...</p>';

  if (!is_dir($_ENV['css_cache_dir'])) {
    $_ENV['css_cache_dir'] = '../assets/css/theme_cache/';
  }

  clearCache($_ENV['css_cache_dir']);

} elseif (isset($_REQUEST['getThemeCSS'])) {
  
  $reqTheme = $_REQUEST['getThemeCSS'];

  $size = "14";
  $weight = "normal";
  $themes = $_ENV['validColorThemes'];
  $sizes = $_ENV['validFontSizes'];
  $css = 'CSS EMPTY';

  foreach ($themes as $t) {
    if ($t == $reqTheme) $css = generateCSS($t, $size, $weight);
  }

  header("Content-Type: text/css");
  
  die($css);
}

echo '<p>Complete.</p>';
echo '<p><a href="?clear_cache">Clear cache</a> | <a href="?cache_all">Build cache</a> | <a href="?cache_css">Build CSS cache</a> | <a href="?cache_css&t=dark">Build dark cache</a></p>';

?>
