<?php
// Write out HTML for QMod, JS tags, etc.

function writeCalendarMenu($clientForwardURL, $wmid, $qmEnv,$pageInfo, $targetURL) {
  echo '<p>Stock Calendars (<a href="'.
    $clientForwardURL .
    '&action=showCalendarSummary&calendarGroup=brokerratings">Broker Ratings</a> | <a href="' .
    $clientForwardURL .
    '&action=showCalendarSummary&calendarGroup=earnings">Earnings</a> | <a href="' .
    $clientForwardURL .
    '&action=showCalendarSummary&calendarGroup=corporateactions">Corporate Actions</a>)</p>';
}

function writeQuoteModule($qmEnv, $pageInfo, $wmid, $targetURL) {
  echo '<script LANGUAGE="javascript" TYPE="text/javascript" src="https://' .
    $qmEnv .
    '.quotemedia.com/quotetools/quoteModule.go?toolWidth=825&symbolLinkTarget=off&symlinks=off&webmasterId='.
    $wmid .
    '&action=' .
    $pageInfo['tool'] .
    '&svg=true&chfrm=3b3b3b&chbrdr=3b3b3b&chbg=pink&chbgch=pink' .
    '&hiddenTabs=all&cp=off&targetURL=' .
    $targetURL .
    '"></script>';
}

function writeQModLoader($qmodLoader=false, $qmEnv='app', $wmid='', $sid='', $qmodVersion='') {
  if (@$_REQUEST['page'] == 'advChart') $sid = "";
  else $sid = 'data-qmod-sid="'.$sid.'" ';
  echo '<script id="qmod" type="application/javascript" ';
  echo 'src="https://';
  echo ($qmodLoader === 'webdev') ? 'webdev.' : '';
  echo 'qmod.quotemedia.com/js/qmodLoader.js" data-qmod-env="';
  echo $qmEnv;
  echo '" data-qmod-wmid="'.$wmid.'" '.$sid.'data-qmod-version="'.$qmodVersion.'"></script>';
}

function appendCodes($url, $list) {
  $listStr = $url;
  foreach ($list as $l) {
    $listStr .= '&codes='.$l;
  }
  return $listStr;
}

function writeQMod_II($qmEnv, $wmid, $sid=false, $name, $pageInfo, $advChartParams) {
  ?>
  <script id="qmodiiLoader" src="https://static.c1.quotemedia.com/qmod/qmodii/v1.0.14/qModii.umd.min.js"></script>
  <script>
    function getUrlVars() {
      var vars = {};
      var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
          vars[key] = value;
      });
      return vars;
    }
    var symbol = decodeURIComponent(getUrlVars()["symbol"]);
    
    if (
      symbol === null ||
      typeof symbol === "undefined" ||
      symbol == 'undefined' ||
      symbol == ''
    ) {
      symbol = 'AMZN,GOOGL'; // Defaults
    }
    
    var qmodWMID = <?= $wmid ?>;
    var qmodComponentDOM = '#<?= $name ?>';
    var qmodComponentName = '<?= $name ?>';
    var qmodComponentOptions = <?= $advChartParams ?>;

    window.addEventListener('QMOD:LOADER:FINISHED', function () {
      qModii.init(qmodWMID, qmodComponentDOM, qmodComponentName, qmodComponentOptions);
    });
  </script>
  <div class="qmodii">
    <h2 class="qmod-heading qmod-hbg">Advanced Charting</h2>
    <div id='advChart'></div>
  </div>
  <?php
}

function getToolParams($pageInfo) {
  if (isset($pageInfo['json_params']['base'])) {
    return $pageInfo['json_params']['base'];
  } elseif ($pageInfo['param'] != false) {
    return $pageInfo['param'];
  } else {
    return '';
  }
}

function setColorProfile($pageInfo, $paramString) {
  if (isset($pageInfo['colorType']) && gettype($pageInfo['colorType']) == 'string') {
    $paramString = str_replace('"#', '"'.$pageInfo['colorType'], $paramString);
  }
  return stripslashes(ltrim(rtrim($paramString, '"'), '"')); // remove " around JSON string - replace with ' later
}

function splitCamelCaseWords($ccWord) {
  $re = '/(?#! splitCamelCase Rev:20140412)
    # Split camelCase "words". Two global alternatives. Either g1of2:
      (?<=[a-z])      # Position is after a lowercase,
      (?=[A-Z])       # and before an uppercase letter.
    | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
      (?=[A-Z][a-z])  # and before upper-then-lower case.
    /x';
  $a = preg_split($re, $ccWord);
  $count = count($a);
  $words = '';

  for ($i = 0; $i < $count; ++$i) {
    $words .= ($words == '') ? $a[$i] : ' '.$a[$i];
  }
  return $words;
}

function writeQModTabs($qmEnv, $wmid, $sid, $tabs) {
  $json = new Services_JSON();
  $qmToolCode = '';

  foreach ($tabs as $t) {
    $tool = $t[0];
    $pageInfo = buildParams($t[1]);
    $params = $pageInfo['json_params']['base'];
    $title = splitCamelCaseWords($pageInfo['title']);
    $qmToolCode .= "\n".'qmCode.qmAddTool(\'' . $tool . '\',\'' . $title . '\',\'' . $params . '\');';
  }
  
  $html = '<div class="qm-wrap"><div id="qmNavigation"></div><div id="qmodTool" data-qmod-tool="" data-qmod-params="" class="qtool"></div><div id="qmodLoader"></div></div>';

  $js = '<script src="./assets/js/qmodTabs.js"></script><script>var qmCode = new qmMultiModule();';
  $js .= $qmToolCode;
  $js .= 'qmCode.qmSetEnv("'.$_ENV['available_params']->qmEnv.'");
    qmCode.qmSetVersion("'.$_ENV['available_params']->qmodVersion.'");
    qmCode.qmSetSid("'.$_ENV['available_params']->sid.'");
    qmCode.qmSetWmid("'.$_ENV['available_params']->wmid.'");
    qmCode.run();</script>';

  echo $html.$js;
}

function writeQMod($qmEnv, $wmid, $sid, $name, $pageInfo=false) {
  $json = new Services_JSON();
  $multiPageInfo = array();
  $pageIndex = 0;

  if ($pageInfo == false) {
    if (stristr($name, ",")) {
      $tools = explode(",", $name);

      foreach($tools as $tool) {
        $params = getDefaultParams($tool);
        $paramsString = $json->encode($params);

        if ($params != false) {
          $multiPageInfo[$pageIndex] = array(
            'tool' => $tool,
            'type' => 'qmod',
            'param' => setColorProfile($pageInfo, $paramString),
            'colorType' => (isset($pageInfo['colorType'])) ? $pageInfo['colorType'] : false
          );

          writeQMod($qmEnv, $wmid, $sid, $tool, $multiPageInfo[$pageIndex]);
          $pageIndex++;
        }
      }
    } else {
      $pageInfo['tool'] = $name;
    }
    $pageInfo['param'] = $_ENV['qmodParams'];

  } else {
    if (is_array($pageInfo['tool'])) {
      foreach ($pageInfo['tool'] as $name) {
        $multiPageInfo[$pageIndex] = buildParams(
          array(
            'tool' => $name,
            'type' => $pageInfo['type'],
            'param' => $pageInfo['param'],
            'colorType' => (isset($pageInfo['colorType'])) ? $pageInfo['colorType'] : false
          )
        );

        writeQMod($qmEnv, $wmid, $sid, $name, $multiPageInfo[$pageIndex]);
        $pageIndex++;
      }
    } else {
      $pageInfo = buildParams($pageInfo);
    }
  }

  if (is_array($pageInfo['tool']) && !in_array($name, $pageInfo['tool'])) {
    // Multi-tool
    echo '<div id="multi_qmod">';
    foreach($pageInfo['tool'] as $t) {
      writeQMod($qmEnv, $wmid, $sid, $t, $pageInfo);
    }
    echo '</div>';

  } else {
    $params = setColorProfile($pageInfo, $json->encode(getToolParams($pageInfo)));
    $tool = (is_array($pageInfo['tool'])) ? $name : $pageInfo['tool'];
    echo '<div id="' . $name . '" data-qmod-tool="' . $tool . '" data-qmod-params=\''. $params . '\' data-qmod-sid="'.$sid.'" data-qmod-wmid="'.$wmid.'" class="qtool fadeIn animated"></div>';

    echo "\r\n\r\n".'<script>loadedQMods.push("'.$name.'");</script>';
  }
}

function writeDevOptions() {
  echo '<div id="dev_form">';

  echo '<div class="row shortcuts">';

  if (strstr($_SERVER['HTTP_HOST'], "local")) {
    echo '<div class="col">
            <a class="btn" href="#" id="open-admin-links"><span class="material-icons">security</span> Admin</a>
          </div>';
  }
  echo '<div class="col">
          <a class="btn" href="#" id="open-demo-params"><span class="material-icons">settings</span> Params</a>
        </div>';
  echo '</div>';

  if (strstr($_SERVER['HTTP_HOST'], "local")) {
    echo '<div class="row admin hidden">';
    echo '<div class="col">
            <a class="btn" href="/qsmodule/research/?demo&qmEnv=uat" target="_top">Renew SID</a>
          </div>';
    echo '<div class="col">
            <a class="btn" href="/qsmodule/research/functions/scssCompiler.php?clear_cache" target="_blank">Clear Cache</a>
          </div>';
    echo '<div class="col">
          <a class="btn" href="/qsmodule/research/functions/scssCompiler.php?cache_all" target="_blank">Build Cache</a>
        </div>';
    echo '<div class="col">
          <a class="btn" href="/qsmodule/research/functions/scssCompiler.php?cache_css&t=dark" target="_blank">Build dark Cache</a>
        </div>';
    echo '<div class="col">
          <a class="btn" href="/qsmodule/research/functions/scssCompiler.php?cache_css" target="_blank">Build CSS Cache</a>
        </div>';
    echo '</div>';
  }

  echo '<div class="row demo hidden">';
  echo '<div class="col s1">
          <div class="input-field">
            <input id="symbol" name="symbol" type="text" value="'.
            $_ENV['available_params']->symbol.
            '">
            <label for="symbol" class="active">Symbol</label>
          </div>
        </div>';
  
  echo '<div class="col"><select id="fontSize" class="browser-default">';
  foreach ($_ENV['validFontSizes'] as $f) {
    $selected = ($_ENV['available_params']->fontSize == $f) ? 'selected' : '';
    echo '<option value="'.$f.'" '.$selected.'>'.$f.'</option>';
  }
  echo '<label for="fontSize">Font Size:</label>';
  echo '</select></div>';
  
  echo '<div class="col"><select id="theme" class="browser-default">';
  foreach ($_ENV['validColorThemes'] as $f) { 
    $selected = ($_SESSION['theme'] == $f) ? 'selected' : '';
    echo '<option value="'.$f.'" '.$selected.'>'.$f.'</option>';
  }
  echo '<label for="theme">Theme:</label>';
  echo '</select></div>';


  echo '<div class="col"><select id="lang" class="browser-default">';
  foreach ($_ENV['validLanguages'] as $l) {
    $selected = ($_SESSION['user_params']['lang'] == $l) ? 'selected' : '';
    echo '<option value="'.$l.'" '.$selected.'>'.$l.'</option>';
  }
  echo '<label for="theme">Lang:</label>';
  echo '</select></div>';

  echo '<div class="col"><select id="country" class="browser-default">';
  foreach ($_ENV['validCountries'] as $c) {
    $selected = ($_SESSION['user_params']['lang'] == $c) ? 'selected' : '';
    echo '<option value="'.$c.'" '.$selected.'>'.$c.'</option>';
  }
  echo '<label for="theme">Country:</label>';
  echo '</select></div>';

  echo '</div>';
  echo '</div>';
}

function listFormat($p='', $link='', $title) {
  if (strstr($p, ",")) {
    $list = explode(",", $p);

    if (count($list) > 1) {
      $html = '';
      $children = '';

      for ($x=0; $x < count($list); $x++) {
        if ($x == 0) {
          $html .= '<a href="'.$link.'" name="'.$p.'"><strong>'.$title.'</strong></a><ul>{CHILDREN}</ul>';

        } else {
          $children .= '<li> <a href="'.$link.'" name="'.$p.'"> + '.$list[$x].'</a></li>';
        }
      }
      return str_replace("{CHILDREN}", $children, $html);
    }
  } else {
    return '<a href="'.$link.'" name="'.$p.'">'.$title.'</a>';
  }
}

function writeUserInputFields() {
  $local = (strstr($_SERVER['SERVER_NAME'], "local")) ? true : false;
  $wmid = ($local) ? '101894' : '';
  $user = ($local) ? 'Iamstillhappy' : '';
  $pass = ($local) ? '101894Pass' : '';

  return '<div class="row">
            <form class="col s12">
              <div class="row">
                <div class="col s6">
                  <div class="input-field col">
                    <input id="wmid" name="wmid" type="text" value="'.$wmid.'">
                    <label for="wmid" class="active">WMID</label>
                  </div>
                  <div class="input-field col">
                    <input id="username" name="username" type="text" value="'.$user.'">
                    <label for="username" class="active">User</label>
                  </div>
                  <div class="input-field col">
                    <input id="password" name="password" type="text" value="'.$pass.'">
                    <label for="password" class="active">Password</label>
                    <a id="get-sid-btn" class="waves-effect waves-light btn">Get SID</a>
                  </div>
                </div>

                <div class="input-field col s6">
                  <input id="sid-input" name="sid-input" type="text" class="validate" value="">
                  <label for="sid-input" class="active">SID</label>
                  <a id="set-sid-btn" class="waves-effect waves-light btn">Set SID</a>
                </div>
              </div>
            </form>
          </div>';
}

function nameSort($a, $b) {
  return strcmp($a['Name'], $b['Name']);
}

function writeMenu($pages) {
  $qmEnv = $_ENV['available_params']->qmEnv;
  $wmid = $_ENV['available_params']->wmid;
  $sid = $_ENV['available_params']->sid;
  $symbol = $_ENV['available_params']->symbol;
  $fontSize = $_ENV['available_params']->fontSize;
  $theme = $_ENV['available_params']->colorTheme;
  $lang = $_ENV['available_params']->lang;
  $country = $_ENV['available_params']->country;

  // ksort($pages);
  usort($pages, "nameSort");

  $pageGroups = array();

  foreach ($pages as $p => $v) {
    if (!in_array($v['Category'], $pageGroups)) $pageGroups[] = $v['Category'];
  }

  sort($pageGroups);

  echo '<div class="demo-menu">';

  if ($sid == '') {
    echo writeUserInputFields();
    return 0;
  }

  // Add sid
  $newUrl = "?demo&qmEnv=$qmEnv&symbol=$symbol&wmid=$wmid&sid=$sid&fontSize=$fontSize&theme=$theme";

  echo '<p class="hidden">SID: <a id="demo-sid" href="'.$newUrl.'">'.$sid.'</a></p>';

  writeDevOptions();

  echo '<div id="demo-preview-container"><div id="qmod-dev-list">';

  foreach ($pageGroups as $g) {
    $headerText = ($g != 'N/A') ? $g : 'Other';

    echo '<h5>'.$headerText.'</h5><ul>';

    foreach ($pages as $p => $v) {
      if ($v['Category'] == $g) {
        
        $linkSymbol = (
          stristr($v['Page'], 'fund') != false &&
          !isset($_REQUEST['symbol'])
        ) ? 'VTSAX' : $symbol;

        if (stristr($p, 'fund')) {
          $linkSymbol = "VTSAX";
        }

        $storyId = '';

        if (trim($v['Page']) == 'fullnewsstory') {
          $storyId = ($qmEnv == 'uat') ? '&storyId=4556601182441685' : '&storyId=7944069352022280';
        }
        
        $link = "?page=".urlencode($v['Page'])."&symbol=$linkSymbol&wmid=$wmid&sid=$sid&qmEnv=$qmEnv&fontSize=$fontSize&theme=$theme&lang=$lang&country=$country".$storyId;

        echo '<li>'.listFormat($v['Page'], $link, $v['Name']).'</li>';
      }
    }
    echo '</ul>';
  }
  echo '</div><div class="frame-container"><iframe id="qsmodule-preview" class="hidden"></iframe></div></div>';

  echo '</div>';
}

?>
