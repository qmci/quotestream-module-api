<?php
function isCanadianSymbol($symbol) {
  return (
    stristr($symbol, ":CA") ||
    stristr($symbol, ":CNX") ||
    stristr($symbol, ":AQL") ||
    stristr($symbol, ":CC")
  ) ? true : false;
}

function getAllEntitlementCodes() {
  $codes = array();

  foreach ($_ENV['entitlement_codes'] as $level => $groups) {
    if (is_array($groups) && count($groups) == 2) {
      foreach($groups as $country => $entitlements) {
        foreach ($entitlements as $entitlement) {
          $codes[] = $entitlement;
        }
      }
    }
  }
  return $codes;
}

function checkEntitlements($symbol) {

  $returnEntitlement = false;
  $json = new Services_JSON();

  $codes = getAllEntitlementCodes();

  if ($_ENV['available_params']->sid == null) {
    $_ENV['error'] = '<p class="error"><strong>Error:</strong> SID is Required</p>';
    return false;
  }

  $url = appendCodes(
    $_ENV['entitlementUrl'],
    $codes
  );
  
  $result = $json->decode(getURL($url));
  
  if (isset($result->error)) {
    $_ENV['error'] = '<p class="error"><strong>Entitlement Error</strong>: '.$result->error.'</pre>';
    return false;
  }

  foreach ($_ENV['entitlement_codes'] as $level => $groups) {
    if (isset($result->userEntitlements)) {
      foreach($groups as $country => $entitlements) {
        foreach ($result->userEntitlements as $e) {
          if (
            $e->isActive === true &&
            $returnEntitlement != 'premium' &&
            in_array($level, $_ENV['validEntitlements'])
          ) {
            foreach ($entitlements as $entitlement) {
              if (
                $e->name == $entitlement &&
                (
                  (
                    $country == 'CA' && isCanadianSymbol($symbol)
                  ) ||
                  (
                    $country != 'CA' && !isCanadianSymbol($symbol)
                  )
                )
              ) {
                $returnEntitlement = $level;
              }
            }
          }
        }
      }
    }
  }

  return $returnEntitlement;
}

function testRequiredParams($required_params) {
  $matches = 0;
  $required = count($required_params);

  $error = '<div class="error"><strong>Missing parameters:</strong><ul>';

  foreach($required_params as $p) {
    $paramMatch = false;

    foreach ($_REQUEST as $r => $v) {
      if ($r == $p) {
        $matches++;
        $paramMatch = true;
      }
    }
    if ($paramMatch == false) $error .= '<li>'.$p.'</li>';
  }
  $error .= '</ul></div>';

  if ($matches != $required) {
    echo $error;
    return false;
  }
  return true;
}

function sessionTest($wmid, $sid='') {
  $json = new Services_JSON();
  $url = $_ENV['check_session'] . $wmid . '?sid=' . $sid;

  $result = $json->decode(getURL($url));

  if (isset($result->sid)) return true;
  return false;
}

function getDemoSID() {
  $wmid = '101894'; // 101894/Iamstillhappy/101894Pass/ try this user
  $user = 'Iamstillhappy'; // Iamstillhappy (LITE) / qmoddemo1 (Premium) 
  $pass = '101894Pass';

  $sid = getSID($wmid, $user, $pass);

  return $sid;
}

function getSID($wmid, $user, $pass) {
  $params = array(
    'wmId' => $wmid,
    'username' => $user,
    'password' => $pass
  );

  foreach ($params as $p => $v) {
    if (!isset($v) || $v == '') {
      die('Error: '.$p. ' is invalid');
    }
  }
  
  $json = new Services_JSON();
  $auth = $json->decode(
    getURL($_ENV['get_sid'], $params)
  );

  if (
    @$auth->code->name == 'Ok' &&
    isset($auth->sid) &&
    (
      $auth->status == 'Active' ||
      $auth->status == 'QA'
    )
  ) {
    return $auth->sid;
    
  } else {
    echo '<p>Error retrieving SID</p>';
  }
  return '';
}

function getURL($url, $params=false) {
  $ch = curl_init();

  $headers = array(
    'Content-Type: application/json',
  );

	if ($params != false) {
    $url = $url . $params['wmId'] .'/' . $params['username'] . '/' . $params['password'].'/';
  }
	
	curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FAILONERROR, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_VERBOSE, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  $result = curl_exec($ch);

	$output = (curl_errno($ch)) ? curl_error($ch) : $result;
  curl_close($ch);

	return $output;
}
?>