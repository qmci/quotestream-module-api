<?php

$cacheDir = $_SERVER['DOCUMENT_ROOT'].'/qsmodule/research/functions/json/cache/';
$keyFilePath = $_SERVER['DOCUMENT_ROOT'].'/qsmodule/research/functions/json/imp-clients-qmods-27c7a6b1cdcb.json';
$prefix = getPrefix($keyFilePath);
$accountKeyFile = $prefix.$keyFilePath;

if (!is_file($accountKeyFile)) die('account key not found @ '.$accountKeyFile);

$_ENV['cache_dir'] = $prefix.$cacheDir;
$_ENV['css_cache_dir'] = $_SERVER['DOCUMENT_ROOT'].'/qsmodule/research/assets/css/theme_cache/';

$_ENV['google'] = array(
  'account' => array(
    'Service Account Key' => $accountKeyFile,
  ),
  'sheets' => array(
    'QMod Colors' => array(
      'id' => '1BgogcvtXCyddmrvJ0owT8Ph1g1w4sSGIc_E0Wr09MYs',
      'url' => 'https://docs.google.com/spreadsheets/d/1BgogcvtXCyddmrvJ0owT8Ph1g1w4sSGIc_E0Wr09MYs/edit',
      'tabs' => array(
        'QMods',
        'QS QMod Modules',
        'Types',
        'Themes',
        'base',
        'qmodii',
        'legend',
        'chart',
        'chartParams',
        'modalParams',
        'topChartParams',
        'fundColorParams',
        'sparkline'
      )
    )
  )
);

function requirePHPVersion($reqVersion=7) {
  $version = explode('.', PHP_VERSION);

  if ($version[0] < $reqVersion) {
    // PHP version failed
    if (!file_exists($_SERVER['DOCUMENT_ROOT'].'/qsmodule/research/functions/vendor/autoload.php')) {
      die('<p>Failed vendor autoload.</p>');
    }
    return false;
  }
  return true;
}

function getPrefix($file) {
  $p = '';
  $count = 0;
  while (!is_file($p.$file) && $count < 4) {
    $count++;
    $p .= '../';
  }
  return $p;
}

function loadQmodsFromSheets() {
  return readFullSheet('QMod Colors');
}

function clearCache($dir=false) {
  $files = scandir($dir);
  $x = 0;
  foreach($files as $file) {
    if(is_file($dir.$file)) {
      unlink($dir.$file);
      $x++;
    }
  }
}

function getCache($filename) {
  $json = new Services_JSON();
  $css = (stristr($filename, '.css')) ? true : false;

  if (is_file($filename)) {
    $data = file_get_contents($filename);
    
    if ($css != true) {
      $arr = (array) $json->decode($data);
    } else {
      $arr = $data;
    }

    return $arr;
  }
  return false;
}

function cacheResult($result, $filename) {
  $css = (stristr($filename, '.css')) ? true : false;
  $json = new Services_JSON();

  if (strstr($_SERVER['HTTP_HOST'], "local") && file_exists($filename)) {
    gc_collect_cycles();
    @unlink($filename); // always destroy cached on local
  }

  if (!is_file($filename)) {
    if ($css == false && file_put_contents($filename, $json->encode($result))) {
      return true;
    } elseif ($css == true && file_put_contents($filename, $result)) {
      return true;
    }
  }
  return false;
}

function sanitizeString($string) {
  if (function_exists('mb_ereg_replace')) {
    $string = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $string);
    $string = mb_ereg_replace("([\.]{2,})", '', $string);
    $string = str_replace(' ', '_', $string);
  } else {
    $string = str_replace(' ', '_', $string);
  }
  return $string;
}

function processSheetData($values) {
  // make it into a nicer array/object
  $headers = array();
  $rArr = array(); // return array

  if (is_array($values[0]) && count($values[0]) > 0) {
    // Array of headers
    for ($x = 0; $x < count($values[0]); $x++) {
      $headers[] = $values[0][$x];
    }
  }

  for ($x = 1; $x < count($values); $x++) {
    if (isset($values[$x]) && count($values[$x]) > 0) {
      $vArr = array();
      $header = null;

      for ($y = 0; $y < count($values[$x]); $y++) {
        if (count($headers) > 1) {
          $vArr[$headers[$y]] = $values[$x][$y];

        } else {
          $rArr[] = $values[$x][$y];
        }
      }
      if (count($headers) > 1) {
        $rArr[] = $vArr;
      }
    }
  }
  return $rArr;
}

function getQModParamsFromSheets() {
  $returnData = array();
  $sheets = array();

  $spreadsheetName = 'QMod Colors';
  $id = $_ENV['google']['sheets'][$spreadsheetName]['id'];
  $tabs = $_ENV['google']['sheets'][$spreadsheetName]['tabs'];

  $ranges = array();
  $sCount = 0;
  $usingCache = 0;

  $missedCache = array();

  for ($x = 0; $x < count($tabs); $x++) {
    $name = $tabs[$x];
    $cache = sanitizeString($name).'_'.$id;
    $range = $name."!A:Z";
    $ranges[] = $range;
    $cached = getCache($_ENV['cache_dir'].$cache.'.json');

    $useCache = false;

    if ($cached != false && @$_REQUEST['clear_cache'] != 'files') {
      $cacheData = $cached;
      $useCache = true;
      $usingCache++;

    } else {
      $cacheData = null;
      $missedCache[] = $name;
    }

    $sheets[$name] = array(
      'id' => $id,
      'range' => $range,
      'cache' => $cache,
      'useCache' => $useCache,
      'values' => $cacheData
    );

    $returnData[$name] = $cacheData; // $sheets[$name]
    // $returnData[$name] = $sheets[$name];
  }

  if ($usingCache == count($tabs)) {
    // Everything is cached
    return $returnData;

  } elseif (requirePHPVersion() == true) {
    requireSheetsApi();
    $client = getClient();

    $service = new Google_Service_Sheets($client);
    
    // Load Sheet Data
    $response = $service->spreadsheets_values->batchGet($id, array('ranges' => $ranges));
    $values = $response->getValueRanges();

    foreach ($sheets as $name => $s) {
      $valueIndex = $sCount;
      $sCount++;

      if ($returnData[$name] == null) $returnData[$name] = array();

      if ($s['useCache'] == true) {
        // echo '<p>Using Cache: '.$name.'</pre><pre>',print_r($s->values),'</pre>';

      } else {
      
        $dataArray = array();
        $header = $values[$valueIndex][0];

        if (count($values[$valueIndex][0]) > 1 && count($header) > 1) {
          $valArr = array();
          
          for ($z = 1; $z < count($values[$valueIndex]['values']); $z++) {
            $i = $z - 1;
            $params = $values[$valueIndex]['values'][$z];
            
            if (is_array($params)) {
              for ($p = 0; $p < count($params); $p++) {
                $returnData[$name]['values'][$i][$header[$p]] = $params[$p];
              }
            }
          }

        } else {
          if (count($header) == 1) {
            for ($z = 1; $z < count($values[$valueIndex]); $z++) {
              if (isset($values[$valueIndex][$z])) {
                $v = $values[$valueIndex][$z];
                for ($p = 0; $p < count($v); $p++) {
                  $val = $values[$valueIndex][$z][$p];
                  $returnData[$name]['values'][] = $val;
                }
              }
            }
          }
        }

        $s['data'] = $returnData[$name];
        $cacheFile = $_ENV['cache_dir'].$s['cache'].'.json';

        if (cacheResult($returnData[$name]['values'], $cacheFile)) {
          // echo '<p>Cache write successful: '.$name.'</p><pre>',print_r($s['data']),'</pre>';
        } else {
          die('cache write failed');
        }
      }
    }
  } else {
    echo '<p>CACHE MISS - REQUIRED PHP VERSION NOT FOUND - CANNOT BUILD CACHE: </p><ul>';
    foreach ($missedCache as $mC) echo '<li>'.$mC.'</li>';
    echo '</ul>';
    die;
  }

  // echo '<pre>',print_r($returnData),'</pre>';die;

  return $returnData;
}

function readFullSheet($sheet='') {
  $client = getClient();
  $service = new Google_Service_Sheets($client);

  $range = '!A1:ZZ';
  $response = $service->spreadsheets_values->get(
    $_ENV['google']['sheets'][$sheet]['id'],
    $range
  );
  $values = $response->getValues();

  return $values;
}

function readSheet($sheet='', $tab='', $rows='') {
  $client = getClient();
  $service = new Google_Service_Sheets($client);

  $range = $tab.'!'.$rows;
  $response = $service->spreadsheets_values->get(
    $_ENV['google']['sheets'][$sheet]['id'],
    $range
  );
  $values = $response->getValues();

  return $values;
}

function formatHeaderText($text='') {
  return trim(
    ucwords(
      str_replace('_', ' ', $text)
    )
  );
}

function writeSheetHeaderRow($client, $sheet, $spreadsheet, $headers=array()) {
  echo '<p>SHEET HEADERS FOUND: '.count($headers).'</pre>';
  $range = "'$spreadsheet'!A1:CR";
  $service = new Google_Service_Sheets($client);
  $spreadsheetId = $_ENV['google']['sheets'][$sheet]['id'];
  $valueRange= new Google_Service_Sheets_ValueRange();
  $params = array('valueInputOption' => 'RAW');
  $ins = array("insertDataOption" => "OVERWRITE");
  // Add values
  $valueRange->setValues(array("values" => $headers));

  // Add new Header Row to Spreadsheet
  $response = $service->spreadsheets_values->update(
    $spreadsheetId,
    $range,
    $valueRange,
    $params,
    $ins
  );
}

function saveParams($qmods) {
  $id = $_ENV['google']['sheets']['QMod Params']['id'];
  $client = getClient();
  $service = new Google_Service_Sheets($client);
  $sheet = $service->spreadsheets->get($id);
  $tabs = $sheet->sheets;

  foreach ($qmods as $q) {
    $match = false;

    foreach ($tabs as $tab) {
      if ($tab->properties->title == $q) {
        $match = true;
      }
    }
    if ($match == false) {
      echo '<br />Add Tab: '.$q;
      
      $params = getQModParams($q);

      // Add a delay, so we don't exceed quota (500 requests per 100 seconds per project, and 100 requests per 100 seconds per user)
      sleep(3);

      // Add new sheet (tab) to Spreadsheet
      $body = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
        'requests' => array('addSheet' => array('properties' => array('title' => $q )))));
    
      $result = $service->spreadsheets->batchUpdate($id, $body);

      // Format params array for sheets
      $rows = array(
        array('Param', 'Default', 'Options')
      );
      if (isset($result->replies[0]->addSheet->properties->sheetId)) {
        foreach ($params as $p) {
          $row = array(
            $p['name'],
            $p['default']
          );
          if (isset($p['options'])) {
            foreach ($p['options'] as $o) {
              $row[] = $o;
            }
          }
          $rows[] = $row;
        }
        // Add a delay, so we don't exceed quota (500 requests per 100 seconds per project, and 100 requests per 100 seconds per user)
        sleep(3);
        appendRowToSheet('QMod Params', $q, 'A1', $rows, $client);

      } else {
        die('<p>ERROR: Sheet could not be created.</p>');
      }
    }
  }
}

function updateSheet($sheet='', $tab='', $rows='', $data, $client=false) {
  if ($client != false) $client = getClient();

  $range = "'$tab'!".$rows;
  $service = new Google_Service_Sheets($client);
  $spreadsheetId = $_ENV['google']['sheets'][$sheet]['id'];
  $valueRange= new Google_Service_Sheets_ValueRange();
  
  $params = array('valueInputOption' => 'USER_ENTERED');
  $ins = array("insertDataOption" => "OVERWRITE");
  $dimension = array('majorDimension' => 'ROWS');

  $insert = array();
  $response = array();

  foreach($data as $d) {
    // sleep(5); // delay if there are a lot of requests, to avoid exceeding request limit

    // Add value
    $valueRange->setValues(array("values" => $d));
    
    // Add new row to Spreadsheet
    $response[] = $service->spreadsheets_values->update(
      $spreadsheetId,
      $range,
      $valueRange,
      $params,
      $ins,
      $dimension
    );
  }
  return $response[0];
}

function appendRowToSheet($sheet='', $tab='', $rows='', $data, $client=false) {
  if ($client != false) $client = getClient();

  echo '<p>Append Row to Sheet ('.$sheet.' > '.$tab.'!'.$rows.')...</p>';
  $range = "'$tab'!".$rows;

  $service = new Google_Service_Sheets($client);
  $spreadsheetId = $_ENV['google']['sheets'][$sheet]['id'];
  $valueRange= new Google_Service_Sheets_ValueRange();
  
  $params = array('valueInputOption' => 'USER_ENTERED');
  $ins = array("insertDataOption" => "INSERT_ROWS");
  $dimension = array('majorDimension' => 'ROWS');

  foreach($data as $d) {
    // Add a delay, so we don't exceed quota (500 requests per 100 seconds per project, and 100 requests per 100 seconds per user)
    sleep(5);

    // Add value
    $valueRange->setValues(array("values" => $d));
    
    // Add new row to Spreadsheet
    $response[] = $service->spreadsheets_values->append(
      $spreadsheetId,
      $range,
      $valueRange,
      $params,
      //$ins,
      $dimension
    );
  }
  return $response;
}

function writeToSheet($client, $sheet='', $spreadsheet='', $form) {
  echo '<p>Submitting Form to Google Sheets...</p>';

  // Convert data to basic array & add header row
  $dataArr = array();
  $headerRow = array();

  foreach ($form['fields'] as $d => $v) {
    $headerRow[] = formatHeaderText($d);
    $dataArr[] = $v;
  }
  writeSheetHeaderRow($client, $sheet, $spreadsheet, $headerRow);
  appendRowToSheet($client, $sheet, $spreadsheet, $dataArr);
  echo '<p>Submission Complete.</p>';
}

function clearRange($client, $sheet, $range) {
  $service = new Google_Service_Sheets($client);
  $requestBody = new Google_Service_Sheets_ClearValuesRequest();
  $response = $service->spreadsheets_values->clear($sheet, $range, $requestBody);
  // echo '<pre>', print_r($response), '</pre>', "\n";
  if ($response->clearedRange != '') return true;
  else return false;
}

?>
