<?php
/* Get tools list from Google Sheets (or Cache) */

function getToolList() {
  $p = getQModParamsFromSheets();
  $tools = (isset($p['QS QMod Modules']['values'])) ? $p['QS QMod Modules']['values'] : $p['QS QMod Modules'];
  $toolsArr = array();

  foreach ($tools as $t) {
    if (is_array($t)) { // Array to Obj
      $tObj = new stdClass();

      foreach ($t as $tName => $tVal) {
        $tObj->{$tName} = $tVal;
      }
      $t = $tObj;
    }

    $t->type = (trim(strtolower($t->Page)) == 'advchart') ? 'qmodii' : 'qmod';

    if ($t->type != 'qmodii') $t->type = (
      trim(strtolower($t->Page)) == 'showcalendarsummary' ||
      trim(strtolower($t->Page)) == 'showiposummary'
    ) ? 'qtQuoteModule' : 'qmod';

    if (isset($t->{'Multi-tool'})) {
      $t->tool = $t->{'Multi-tool'};

    } else {
      $t->tool = $t->Page;
    }
    $toolHolder = array();

    foreach ($t as $key => $val) {
      $toolHolder[$key] = $val;
    }

    $toolsArr[trim($t->Page)] = $toolHolder;
  }
  return $toolsArr;
}
?>
