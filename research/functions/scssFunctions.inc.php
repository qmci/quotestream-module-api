<?php
require_once('vendor/autoload.php');

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\Server\Server;

function cacheAllThemes() {
  $themes = $_ENV['validColorThemes'];
  $sizes = $_ENV['validFontSizes'];

  foreach ($themes as $t) {
    if (!isset($_REQUEST['t']) || $_REQUEST['t'] == $t) {
      foreach ($sizes as $s) {
        $size = $_ENV['fontVal'][$s]['px'];
        $weight = $_ENV['fontVal'][$s]['weight'];
        generateCSS($t, $size, $weight, true);
      }
    }
  }
}

function generateCSS($theme, $size, $weight, $cacheOnly=false) {
  if (!is_dir($_ENV['css_cache_dir'])) {
    $_ENV['css_cache_dir'] = '../assets/css/theme_cache/';
  }
  $cache_filename = $_ENV['css_cache_dir'].$theme.'_'.$size.'_'.$weight.'.css';

  if ($cacheOnly == false) header("Content-Type: text/css");

  if (is_file($cache_filename) && !strstr($_SERVER['HTTP_HOST'], 'local')) {
    // return cached
    echo '/* cached */';

    return getCache($cache_filename);

  } else {
    if (!isset($_SESSION['themes'])) {
      requireSheetsApi();
    }

    $colorsFile = '../assets/css/qmod/theme.colors.inc.scss';
    $themeFile = '../assets/css/qmod/theme.scss';
    $files = array($colorsFile, $themeFile);
    $themeMap = $_ENV['css_cache_dir'].'theme.map';

    try {
      $scss = new Compiler();

      if (file_exists($themeMap)) @unlink($themeMap = $_ENV['css_cache_dir'].'theme.map');

    } catch (\Exception $e) {
      die('<p>Error: Unable to compile SCSS content</pre>');
    }

    $app = $_ENV['google']['account']['Service Account Key'];

    $themeParams = getThemes();
    $themeVars = getThemeVars($theme, $themeParams);
    $generatedSCSS = applyThemeToScssVars($themeVars, $size, $weight, $files);
    $scss->setImportPaths('./');
    $scss->setFormatter('ScssPhp\ScssPhp\Formatter\Expanded'); // Expanded, Nested (default), Compressed, Crunched
    $scss->setLineNumberStyle(Compiler::LINE_COMMENTS);
    $scss->setSourceMap(Compiler::SOURCE_MAP_FILE);
    $scss->setSourceMapOptions(array(
        // absolute path to write .map file
        'sourceMapWriteTo'  => $themeMap,
        // relative or full url to the above .map file
        'sourceMapURL'      => '/qsmodule/research/assets/css/theme_cache/theme.map',
        // (optional) relative or full url to the .css file
        'sourceMapFilename' => 'theme.php',
        // partial path (server root) removed (normalized) to create a relative url
        'sourceMapBasepath' => '/qsmodule/research/assets/css/theme_cache/',
        // (optional) prepended to 'source' field entries for relocating source files
        'sourceRoot'        => '/',
    ));

    $css = $scss->compile($generatedSCSS);
    
    cacheResult($css, $cache_filename);
    
    return $css;
  }
}
?>
