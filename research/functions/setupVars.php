<?php
$required_params = array('wmid', 'sid', 'page');

$_ENV['validQmEnvs'] = array('dev','stage','qa','uat','app');
$_ENV['validEntitlements'] = array('lite','premium');
$_ENV['validColorThemes'] = array('dark','darkrb','light','lightrb','classic','blue');
$_ENV['validFontSizes'] = array('small','medium','mediumBold','large','largeBold','larger','largerBold','largest','largestBold');
$_ENV['validLanguages'] = array('en', 'fr');
$_ENV['validCountries'] = array('US', 'CA');
$_ENV['fund_entitlement_checks'] = array('fundoverview', 'fundprofile', 'fundperformance', 'fundholdings', 'fundsectorholdings');

$pageInfo = getToolList();

// Get Query Params & Set Defaults
$wmid = isset($_REQUEST['wmid']) ? htmlspecialchars($_REQUEST['wmid']) : '501';
$sid = isset($_REQUEST['sid']) ? $_REQUEST['sid'] : '';
$qmEnv = (isset($_REQUEST['qmEnv']) && in_array($_REQUEST['qmEnv'], $_ENV['validQmEnvs'])) ? $_REQUEST['qmEnv'] : 'app';
$_ENV['get_sid'] = 'https://'.$qmEnv.'.quotemedia.com/auth/g/authenticate/v0/';

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FOR DEMO USE ONLY (remove for production)
//
// $sid = (isset($_REQUEST['demo']) && !isset($_REQUEST['sid'])) ? getDemoSID() : $sid; // REMOVE IN PRODUCTION
// $wmid = (isset($_REQUEST['demo']) && !isset($_REQUEST['wmid'])) ? '101894' : $wmid; // REMOVE IN PRODUCTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////

$_ENV['entitlementUrl'] = 'http://'.$qmEnv.'.quotemedia.com/ums/account/webmaster/'.$wmid.'/user/entitlements?sid='.$sid;
$_ENV['get_params'] = 'http://qmod.quotemedia.com/getsettings/'; // + /toolname (for individual tool params)
$_ENV['check_session'] = 'http://'.$qmEnv.'.quotemedia.com/auth/v0/session/';

$symbol = (isset($_REQUEST['symbol']) ? htmlspecialchars($_REQUEST['symbol']) : 'AAPL');

$qmodVersion = isset($_REQUEST['qmodVersion']) ? htmlspecialchars($_REQUEST['qmodVersion']) : 'v1.21.0';
$qmodLoader = (isset($_REQUEST['qmodLoader']) && $_REQUEST['qmodLoader'] == 'webdev') ? 'webdev' : '';
$country = (isset($_REQUEST['country']) && strtolower($_REQUEST['country']) == 'ca') ? 'CA' : 'US';
$lang = (isset($_REQUEST['lang']) && $_REQUEST['lang'] == 'fr') ? 'fr' : 'en';

$pFile = ($country == 'CA') ? 'ca_modified_qmod_params.json' : 'modified_qmod_params.json'; // Modified default params (CA/US)
$paramsFile = $_SERVER['DOCUMENT_ROOT'].'/qsmodule/research/functions/json/qmod/params/'.$pFile;
if (file_exists($paramsFile)) $_ENV['json_params'] = $paramsFile;
else die('JSON Params not found');

if (isset($_REQUEST['theme']) || !isset($_SESSION['theme'])) {
  $_SESSION['theme'] = (isset($_REQUEST['theme']) && in_array($_REQUEST['theme'], $_ENV['validColorThemes'])) ? $_REQUEST['theme'] : 'dark';
}
if (isset($_REQUEST['fontSize']) || !isset($_SESSION['user_params'])) {
  $fontSize = (isset($_REQUEST['fontSize']) && in_array($_REQUEST['fontSize'], $_ENV['validFontSizes'])) ? $_REQUEST['fontSize'] : 'medium';
} else {
  $fontSize = (isset($_SESSION['user_params']['fontSize']) && in_array($_SESSION['user_params']['fontSize'], $_ENV['validFontSizes'])) ? $_SESSION['user_params']['fontSize'] : 'medium';
}
if (isset($_REQUEST['fontSize']) || !isset($_SESSION['user_params'])) {
  $fontWeight = (stristr($fontSize, "bold")) ? 'bold' : 'normal';
} else {
  $fontWeight = $_SESSION['user_params']['fontWeight'];
}

$_ENV['available_params'] = new stdClass();
$_ENV['available_params']->qmEnv = $qmEnv;
$_ENV['available_params']->wmid = $wmid;
$_ENV['available_params']->sid = $sid;
$_ENV['available_params']->symbol = $symbol;
$_ENV['available_params']->qmodVersion = $qmodVersion;
$_ENV['available_params']->colorTheme = $_SESSION['theme'];
$_ENV['available_params']->fontSize = $fontSize;
$_ENV['available_params']->fontWeight = $fontWeight;
$_ENV['available_params']->qmodLoader = $qmodLoader;
$_ENV['available_params']->country = $country;
$_ENV['available_params']->lang = $lang;

$_SESSION['qmod_params'] = getParamsFromWeb();

$_ENV['qmod_param_overrides'] = new stdClass();
// REMOVED to avoid conflict with param updates in WEBDEV-1317
// $_ENV['qmod_param_overrides']->mktIndices = new stdClass();
// $_ENV['qmod_param_overrides']->mktIndices = array('mktIndicesTSX', false); // Override if ?mktIndicesTSX=true
// $_ENV['qmod_param_overrides']->mktIndicesTSX = new stdClass();
// $_ENV['qmod_param_overrides']->mktIndicesTSX = array('mktIndicesTSX', true); // Default
$defaultStoryId = ($qmEnv == 'uat') ? '6440928357283638' : '7944069352022280'; // default for demo purposes

$_ENV['qmod_param_overrides']->mktDiary = new stdClass();
$_ENV['qmod_param_overrides']->mktDiary = array('any', false); // don't use mktDiary - it breaks the tool
$_ENV['qmod_param_overrides']->storyId = (object) array(
  'param' => 'storyId',
  'value' => (isset($_REQUEST['storyId'])) ? (int) $_REQUEST['storyId'] : $defaultStoryId, // default (can be set by sector param)
  'rule_enabled' => (@$_REQUEST['page'] == 'fullnewsstory') ? true : false, // When to enable this rule
  'request' => 'storyId', // request parameter name
);
$_ENV['qmod_param_overrides']->peerSymbol = (object) array(
  'param' => 'peerSymbol',
  'request' => 'symbol', // request parameter name
  'symbol' => $symbol,
  'disable' => 'sector'
);
$_ENV['qmod_param_overrides']->sector = (object) array(
  'param' => 'sector',
  'request' => 'sector', // request parameter name
  'value' => (isset($_REQUEST['sector'])) ? $_REQUEST['sector'] : 101, // default (can be set by sector param)
  'disable' => 'peerSymbol'
);
$_ENV['qmod_param_overrides']->marketIdEnabled = (object) array(
  'param' => 'marketIdEnabled',
  'rule_enabled' => (@$_REQUEST['page'] == 'qs-etfs') ? true : false, // When to enable this rule
  'request' => false, // ignore request vars
  'value' => false,
  'enable' => (object) array(
    'marketId' => 16 // enable/set additional params
  )
);

$_ENV['fontVal'] = array(
  'small' => array(
    'size' => '10px',
    'px' => 10,
    'weight' => 'normal'
  ),
  'medium' => array(
    'size' => '12px',
    'px' => 12,
    'weight' => 'normal'
  ),
  'mediumBold' => array(
    'size' => '12px',
    'px' => 12,
    'weight' => 'bold'
  ),
  'large' => array(
    'size' => '14px',
    'px' => 14,
    'weight' => 'normal'
  ),
  'largeBold' => array(
    'size' => '14px',
    'px' => 14,
    'weight' => 'bold'
  ),
  'larger' => array(
    'size' => '16px',
    'px' => 16,
    'weight' => 'normal'
  ),
  'largerBold' => array(
    'size' => '16px',
    'px' => 16,
    'weight' => 'bold'
  ),
  'largest' => array(
    'size' => '18px',
    'px' => 18,
    'weight' => 'normal'
  ),
  'largestBold' => array(
    'size' => '18px',
    'px' => 18,
    'weight' => 'bold'
  ),
);

$lite_entitlement_codes = array(
  'CA' => array('QM_CDN_TEASER_DATA_SNAP','MS_CDN_TEASER_DATA_SNAP','FD_TEASER_DATA_SNAP'),
  'US' => array('QM_US_TEASER_DATA_SNAP','MS_US_TEASER_DATA_SNAP',)
);
$premium_entitlement_codes = array(
  'CA' => array('QM_CDN_PREM_DATA_SNAP','MS_CDN_PREM_DATA_SNAP','FD_PREM_DATA_SNAP'),
  'US' => array('QM_US_PREM_DATA_SNAP','MS_US_PREM_DATA_SNAP')
);

$_ENV['entitlement_codes'] = array(
  'lite' => $lite_entitlement_codes,
  'premium' => $premium_entitlement_codes
);

$advChartParams = "{'PropSymbols':'$symbol','PropRemovableSymbolCount':1,'PropEnable20Year':true}";

$_SESSION['user_params'] = array(
  'lang' => $lang,
  'country' => $country,
  'symbol' => $symbol,
  'fontSize' => $fontSize,
  'size' => $_ENV['fontVal'][$fontSize]['size'],
  'chartParams' => array(
    'chfnts' => $_ENV['fontVal'][$fontSize]['px']
  ),
  'fontWeight' => $_ENV['fontVal'][$fontSize]['weight']
);

$page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';

$type = (!is_array($page) && isset($pageInfo[$page]['type'])) ? $pageInfo[$page]['type'] : false;

/* If page uses quoteModule.go, invoke the clientForward redirect (if it hasn't been called already) so that quoteModule.go loads to the correct page */
if ($type == 'qtQuoteModule') {
  // $clientForward = (isset($_REQUEST['clientForward']) && $_REQUEST['clientForward'] == 'false') ? false : true;
  $clientForward = false;
	$targetURL = 'https://' . $_SERVER['HTTP_HOST'] . urlencode($_SERVER['REQUEST_URI']);
  $clientForwardURL = 'https://' . $qmEnv . '.quotemedia.com/quotetools/clientForward?action=' . $pageInfo[$page]['tool'] . '&targetURL=' .  $targetURL;
  
	if ($clientForward == true) {
		header('Location: ' . $clientForwardURL . urlencode('&clientForward=false'));
	}
}
?>
