<?php

function cleanParamVal($val, $originalVal=false) {
  if (!is_array($val) && strlen($val) != 6 && strlen($val) != 7) {
    return $val;
  }
  $returnVals = array();
  
  if (!is_array($val)) {
    $val = array($val);
  }

  foreach ($val as $v) {
    $fC = substr($v, 0, 1);
    
    if (!strstr(@$originalVal, "#")) {
      if ($fC == '#') {
        $returnVal = str_replace("#", "", $v);
        $returnVals[] = $returnVal;

      } else {
        die('<p>no hash</p>');
      }
    } else {
      if (!strstr($v, '#')) $v = '#'.$v;
      $returnVals[] = $v;
    }
  }

  // echo '<p>CLEANED: ',print_r($returnVals),'</p>';

  if (count($returnVals) > 1) return $returnVals;
  else return $returnVals[0];
}

function checkPlurals($param, $params) {
  // if not charts, check chart
  if (substr($param, -1) == "s" && isset($params->{rtrim($param, 's')})) return rtrim($param, 's');
  else return $param;
}

function buildParams($pageInfo) {
  $json = new Services_JSON();
  $themeParams = getThemes();
  $theme = $_SESSION['theme'];
  $themeParamArr = array('base', 'qmodii', 'legend', 'chart', 'chartParams', 'modalParams', 'topChartParams', 'fundColorParams', 'sparkline');
  $cached = $_SESSION['cached'];

  $NO_HASH_TAG = array('detailedquotetabchart'); // these tools don't want # in their color params

  $paramsArr = extractThemeArr($theme, $themeParamArr, $cached);

  if (!isset($_SESSION['theme'])) {
    die('theme not set in session');
  }
  
  // $params = getThemeVars($_SESSION['theme'], $themeParams); // scss vars
  $params = $cached;
  
  $pageInfo['theme'] = $theme;
  $pageInfo['theme_params'] = $paramsArr;
  $themeBaseParams = $_SESSION['cached']['base'];
  $disabledParams = array();

  // echo '<p>THEMES:</p><pre>',print_r($_SESSION['cached']),'</pre>';die;
  // echo '<p>PARAMS:</p><pre>',print_r($themeBaseParams),'</pre>';
  
  if (isset($_SESSION['qmod_params']->paramList->{$pageInfo['tool']}->params)) {
    $defaultParams = $_SESSION['qmod_params']->paramList->{$pageInfo['tool']}->params;
  }

  // echo '<p>THEME PARAMS:</p><pre>',print_r($paramsArr),'</pre>';
  // echo '<p>DEFAULT PARAMS:</p><pre>',print_r($defaultParams),'</pre>';die;

  // echo '<p>DEFAULT PARAMS: '.$theme.'</p><pre>',print_r($defaultParams),'</pre>';
  // echo '<p>OVERRIDES: '.$theme.'</p><pre>',print_r($_ENV['qmod_param_overrides']),'</pre>';

  $toolParams = new stdClass();

  foreach ($defaultParams as $param => $value) {
    $toolParams->{$param} = $value;
    
    $origParam = $param;
    $param = checkPlurals($param, $paramsArr);

    if (is_object($value)) {
      $value = (array) $value;
    }

    if (is_object($value)) {
      foreach($value as $key => $val) {
        if (is_object($val) && in_array($key, $themeParamArr)) {
          foreach($val as $k => $v) {
            foreach ($params[$key] as $themeParam => $themeVal) {
              if ($k == $themeParam) {
                $toolParams->{$origParam}->{$key}->{$themeParam} = cleanParamVal($themeVal, $v);
              }
            }
          }
        } else {
          if (isset($paramsArr[$param])) {
            foreach ($paramsArr[$param] as $themeParam => $themeVal) {

              if ($themeParam == $key) {
                if (is_array($themeVal) && is_array($val)) {
                  for ($x = 0; $x < count($themeVal); $x++) {
                    if (trim(@$themeVal[$x]) != trim(@$val[$x])) {
                      $toolParams->{$origParam}->{$key}[$x] = cleanParamVal($themeVal[$x], @$val[$x]);
                    }
                  }
                } elseif (is_object($val)) {
                  
                  die('OBJECT VALUE FOUND - CHECK FOR THEME MATCH');
                
                } else {

                  if (array_key_exists($key, $_SESSION['user_params']) && $_SESSION['user_params'][$key] != $val) {
                    $toolParams->{$origParam}->{$key} = $_SESSION['user_params'][$key];

                  } else {
                    if (isset($toolParams->{$origParam}->{$key}) && $toolParams->{$origParam}->{$key} != $themeVal) {

                      if (
                        gettype($toolParams->{$origParam}->{$key}) == 'boolean' &&
                        gettype($themeVal) == 'string'
                      ) {
                        if ($themeVal == 'true' || $themeVal == 'TRUE') $themeVal = true;
                        else $themeVal = false;
                      }
                      
                      if (gettype($toolParams->{$origParam}->{$key}) == gettype($themeVal)) {
                        $toolParams->{$origParam}->{$key} = cleanParamVal($themeVal, $toolParams->{$origParam}->{$key});

                      } else {
                        if (gettype($toolParams->{$origParam}->{$key}) == 'integer') {
                          $numVal = (int) $themeVal;
                          $toolParams->{$origParam}->{$key} = $numVal;

                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } elseif(is_string($value) || is_numeric($value) || is_bool($value)) {
      $skipParam = false;

      if (array_key_exists($param, $_ENV['qmod_param_overrides'])) {
        if (gettype($_ENV['qmod_param_overrides']->{$param}) == 'object') {
          $vars = get_object_vars($_ENV['qmod_param_overrides']->{$param});

          if (
            !isset($_ENV['qmod_param_overrides']->{$param}->rule_enabled) ||
            (
              isset($_ENV['qmod_param_overrides']->{$param}->rule_enabled) &&
              $_ENV['qmod_param_overrides']->{$param}->rule_enabled == true
            )
          ) {
            foreach ($vars as $key => $val) {

              if (
                isset($_ENV['qmod_param_overrides']->{$param}->value) &&
                $_ENV['qmod_param_overrides']->{$param}->value != $toolParams->{$param}
              ) {
                if (is_numeric($_ENV['qmod_param_overrides']->{$param}->value)) {
                  $newVal = $_ENV['qmod_param_overrides']->{$param}->value;

                } else {
                  $newVal = ($_ENV['qmod_param_overrides']->{$param}->value == true) ? 'TRUE' : 'FALSE';
                }

                $toolParams->{$param} = $newVal;
              
              } elseif ($key == 'request' && isset($_REQUEST[$val])) {
                $toolParams->{$vars['param']} = $_REQUEST[$val];

              } elseif ($key == 'enable' && gettype($val) == 'object') {
                foreach ($val as $k => $v) {
                  $toolParams->{$k} = $v;
                }
              } elseif ($key != 'rule_enabled') {
                $toolParams->{$param} = $_ENV['qmod_param_overrides']->{$param}->value;
              }
            }

            if (isset($vars['disable'])) {

              if (isset($toolParams->{$vars['disable']})) {
                $disabledParams[] = $vars['disable'];
              }
            }
          }

        } else {

          $overrideRequest = $_ENV['qmod_param_overrides']->{$param}[0];
          $overrideMatchValue = $_ENV['qmod_param_overrides']->{$param}[1];

          if (
            (
              isset($_REQUEST[$overrideRequest]) &&
              $_REQUEST[$overrideRequest] == $overrideMatchValue
            ) ||
            (
              $overrideMatchValue == false &&
              $overrideRequest != 'any' &&
              !isset($_REQUEST[$overrideRequest])
            )
          ) {
            $toolParams->{$param} = $overrideMatchValue;

          } else {
            if (isset($toolParams->{$origParam})) unset($toolParams->{$origParam});
            
          }
        }
      }

      if (gettype($_SESSION['cached']['base']) == 'object') {
        $_SESSION['cached']['base'] = (array) $_SESSION['cached']['base'];
      }

      if (gettype($_SESSION['cached']['base']) == 'array') {
        $match = false;

        foreach ($_SESSION['cached']['base'] as $propObj) {
          if (gettype($propObj) == 'array') {
            $propObj = (object) $propObj;
          }
          if (@$propObj->Prop == $param) {
            $match = true;

            if ($propObj->{$theme} != $value) {
              $toolParams->{$param} = $propObj->{$theme};
            }
          }
        }
        if ($match == false) {
          if (!isset($toolParams->{$param})) $toolParams->{$param} = $value;
        }

      } else {
        foreach ($_SESSION['cached']['base']['values'] as $themeParam => $themeVal) {
          if ($themeParam == $param) {
            if ($value != $themeVal) {
              $toolParams->{$origParam} = $themeVal;
              $toolParams->{$origParam} = cleanParamVal($themeVal, $value);
            }
          }
        }
      }
    } elseif (is_array($value)) {
      if (isset($paramsArr[$param])) {
        foreach ($paramsArr[$param] as $themeParam => $themeVal) {

          if (is_array($themeVal) && is_array($val)) {
            for ($x = 0; $x < count($themeVal); $x++) {
              if (trim(@$themeVal[$x]) != trim(@$val[$x])) {
                $toolParams->{$origParam}->{$key}[$x] = cleanParamVal($themeVal[$x], @$val[$x]);
              }
            }
          } elseif (is_object($value)) {
            
            die('OBJECT VALUE FOUND - CHECK FOR THEME MATCH');
          
          } else {

            if (array_key_exists($themeParam, $_SESSION['user_params']) && $_SESSION['user_params'][$themeParam] != $value) {
              $toolParams->{$origParam}->{$themeParam} = $_SESSION['user_params'][$themeParam];

            } else {
              if (isset($toolParams->{$origParam}->{$themeParam}) && $toolParams->{$origParam}->{$themeParam} != $themeVal) {

                if (
                  gettype($toolParams->{$origParam}->{$themeParam}) == 'boolean' &&
                  gettype($themeVal) == 'string'
                ) {
                  if ($themeVal == 'true' || $themeVal == 'TRUE') $themeVal = true;
                  else $themeVal = false;
                }
                
                if (gettype($toolParams->{$origParam}->{$themeParam}) == gettype($themeVal)) {
                  $toolParams->{$origParam}->{$themeParam} = cleanParamVal($themeVal, $toolParams->{$origParam}->{$themeParam});

                } else {
                  if (gettype($toolParams->{$origParam}->{$themeParam}) == 'integer') {
                    $numVal = (int) $themeVal;
                    $toolParams->{$origParam}->{$themeParam} = $numVal;

                  }
                }
              }
            }
          }
        }
      } else {
        $toolParams->{$param} = $value;
      }
    } else {
      die('UNHANDLED value type: '.gettype($value));
    }
  }

  foreach ($_SESSION['user_params'] as $param => $value) {
    if (array_key_exists($param, $defaultParams)) {
      if (is_array($value)) {
        foreach ($value as $p => $v) {
          if (array_key_exists($p, $defaultParams->{$param})) {
            $defaultParams->{$param}->{$p} = $v;
          }
        }
      } else {
        $toolParams->{$param} = $value;
      }
    }
  }

  if (substr(trim($pageInfo['param']), 0, 1) == '{') {
    $params = $pageInfo['param'];
  } else {
    $params = '{'.$pageInfo['param'].'}';
  }

  // Override Params from URL to Tool Params
  $disabled = array();

  foreach ($_ENV['qmod_param_overrides'] as $oKey => $oVal) {
    if (gettype($oVal) == 'object') {
      if (
        isset($_REQUEST[$oVal->request]) &&
        !in_array($oKey, $disabled) &&
        $oVal->request != false &&
        isset($toolParams->{$oVal->param})
      ) {
        $overrideVal = $_REQUEST[$oVal->request];

        if (isset($oVal->param)) {
          if (isset($toolParams->{$oVal->param})) {
            $toolParams->{$oVal->param} = $overrideVal;

          } else {
            $toolParams->{$oVal->param} = $overrideVal;
          }
          if (isset($oVal->disable)) {
            $disabled[] = $oVal->disable;

            if (isset($toolParams->{$oVal->disable})) {
              unset($toolParams->{$oVal->disable});
            }
          }
        }
      }
    }
  }

  foreach ($_ENV['fund_entitlement_checks'] as $f) {
    if ($f == $pageInfo['tool']) {
      if (
        isset($_REQUEST['entitlement']) &&
        in_array($_REQUEST['entitlement'], $_ENV['validEntitlements'])
      ) {
        $toolParams->entitlement = $_REQUEST['entitlement'];

      } else {
        $entitlement = checkEntitlements($_ENV['available_params']->symbol);
        if ($entitlement != false) $toolParams->entitlement = $entitlement;
      }
    }
  }

  // echo '<p>DISABLE:</p><pre>',print_r($disabledParams),'</pre>';
  // echo '<p>FINAL PARAMS:</p><pre>',print_r($toolParams),'</pre>';die;
  
  // $origParams = $json->decode($params);
  $returnVal = $json->encode($toolParams);

  if (in_array($pageInfo['tool'], $NO_HASH_TAG)) {
    // echo '<p>REMOVE HASHTAGS:</p>';
    $returnVal = str_replace("#", "", $returnVal);
  }
  
  $pageInfo['json_params']['base'] = $returnVal;

  return $pageInfo;
}

function extractThemeArr($theme, $themeParamArr, $themeParams) {

  // echo '<p>Extract theme: ',$theme,'</p>';
  // echo '<p>Extract theme: <pre>',print_r($themeParamArr),'</pre></p>';
  // echo '<p>Extract theme: <pre>',print_r($themeParams),'</pre></p>';die;

  $props = array();

  $defaultTheme = 'dark';

  foreach ($themeParamArr as $pArr) {
    $propName = $pArr;

    if (is_object($themeParams[$pArr])) $themeParams[$pArr] = (array) $themeParams[$pArr];

    if (is_array($themeParams[$pArr])) {
      // $props[$propName] = array();

      foreach($themeParams[$pArr] as $prop) {
        $prop = (gettype($prop) != 'array') ? (array) $prop : $prop; // convert to array, if necessary

        if (@$propName != 'base') {
          if (gettype($prop) == 'array') {
            
            // echo '<p>ARRAY: '.$propName.'</p><pre>',print_r($prop),'</pre><pre>',print_r($themeParams[$pArr]),'</pre>';

            if (isset($themeParams[$pArr]['values'])) {
              for ($x=0; $x < count($themeParams[$pArr]['values']); $x++) {
                $propName = $themeParams[$pArr]['values'][$x]['Prop'];
                $propVal = $themeParams[$pArr]['values'][$x][$theme];
                $props[$pArr][$propName] = $propVal;
              }
            } else {
              for ($x=0; $x < count($themeParams[$pArr]); $x++) {
                $propName = $themeParams[$pArr][$x]->Prop;
                $propVal = $themeParams[$pArr][$x]->{$theme};
                $props[$pArr][$propName] = $propVal;
              }
            }
          } else {
            $name = (gettype($prop) == 'array' && isset($prop['Prop'])) ? $prop['Prop'] : $prop->Prop;
            $value = (gettype($prop) == 'array') ? $prop[$theme] : $prop->{$defaultTheme};

            if (strstr($name, ":")) {
              $names = explode(":", $name);
            }
            
            if (isset($names)) {
              if ($prop->{$defaultTheme} == 'base') {
                $props[$propName][$names[0]][$names[1]] = $themeParams['base']['values'];
              }
            
            } else {
              if (isset($props[$propName][$name])) {
                $tempVal = $props[$propName][$name];

                if (is_array($props[$propName][$name])) {
                  $props[$propName][$name][] = (isset($prop[$theme])) ? $prop[$theme] : $prop[$defaultTheme];
                } else {
                  unset($props[$propName][$name]);
                  $props[$propName][$name] = $value;
                }
              } else {
                $props[$propName][$name] = $value;
              }
            }
          }
        } else {
          // base prop

          // echo '<pre>BASE PROPS: </pre><pre>',print_r($prop),'</pre>';
          // echo '<p>BASE PROP COUNT: </p><pre>',count($prop),'</pre>';
          // echo '<p>THEME PROPS</p><pre>',print_r($themeParams[$pArr]['values']),'</pre>';die;

          if ($propName == 'base') {
            if (isset($prop[0]['Prop'])) {
              for ($x=0; $x < count($themeParams[$pArr]['values']); $x++) {
                $propName = $themeParams[$pArr]['values'][$x]['Prop'];
                $propVal = $themeParams[$pArr]['values'][$x][$theme];
                $props[$pArr][$propName] = $propVal;
              }
            } else {

              $propVal = $prop[$theme];
              $props[$propName][$prop['Prop']] = $propVal;              
            }
          } elseif (isset($props[$propName][@$prop['Prop']])) {

            if (is_array($props[$propName][$prop['Prop']])) {
              $props[$propName][$prop['Prop']][] = $prop[$theme];

            } elseif (gettype($props[$propName][$prop['Prop']]) == 'string') {
              $tempVal = $props[$propName][$prop['Prop']];
              unset($props[$propName][$prop['Prop']]);
              $props[$propName][$prop['Prop']] = array($tempVal, );
            }

          } elseif (@$prop['Prop'] == 'base') {
            $props[$prop['Prop']][$name] = $props[$prop[$theme]];

          } else {
            echo '<p>no match: </p><pre>',print_r($prop),'</pre>';
            die('base prop not found: '.$propName);
          }
        }
      }
    } else {
      // echo('<pre>no params found</pre>');
    }
  }

  // echo 'THEME PROPS:<p>'.$theme.'</pre><pre>',print_r($props),'</pre>';die;

  return $props;
}

function requireSheetsApi() {
  $baseDir = (is_file('functions/vendor/autoload.php')) ? 'functions/' : '../../../functions/';
  $baseDir = (is_file('vendor/autoload.php')) ? '' : $baseDir;

  require_once($baseDir.'vendor/autoload.php');
  require_once($baseDir.'Google.api.php');
  require_once($baseDir.'Google.Auth.api.php');
}

function getThemes() {
  if (!isset($_SESSION['themes'])) {
    $_SESSION['cached'] = getQModParamsFromSheets();
  }
  return $_SESSION['cached']['Themes'];
}

function getParamsFromWeb() {
  if (
    isset($_REQUEST['clear_cache']) &&
    isset($_SESSION['qmod_params'])
  )  {
    unset($_SESSION['qmod_params']);
    unset($_SESSION['themes']);
    
    if (
      @$_REQUEST['clear_cache'] == 'files' &&
      strstr($_SERVER['SERVER_NAME'], "local") // Only do this locally, as we need PHP 7+
    ) {
      clearCache($_ENV['cache_dir']);
    }
  }

  if (!isset($_SESSION['qmod_params']) || !isset($_SESSION['param_country']) || @$_SESSION['param_country'] != $_ENV['available_params']->country) {
    if (file_exists($_ENV['json_params'])) {
      $_SESSION['param_country'] = $_ENV['available_params']->country;
      return getParamsFromFile();
    } else {
      die('Cache failure: File not found.');
    }
  } else {
    return $_SESSION['qmod_params'];
  }
}

function getParamsFromFile() {
  // $json = new Services_JSON();
  $content = file_get_contents($_ENV['json_params']);
  // $paramsObj = $json->decode($content);
  $paramsObj = json_decode2($content);

  return $paramsObj;
}

function isQMod($toolName) {
  return in_array($toolName, $_SESSION['qmod_params']->tools);
}

function getDefaultTitle($toolName) {
  if (isQMod($toolName)) {
    return $_SESSION['qmod_params']->paramList->{$toolName}->name;

  } else {
    return '';
  }
}

function getDefaultParams($toolName) {
  if (isQMod($toolName)) {
    return $_SESSION['qmod_params']->paramList->{$toolName}->params;

  } else {
    die('not qmod: '.$toolName);
    return false;
  }
}

function getThemeVars($theme, $themeVals) {

  for ($x=0; $x < count($themeVals); $x++) {
    if (
      gettype($themeVals[$x]) == 'array'
      && @$themeVals[$x]['Name'] == $theme
    ) {
      $returnObj = new stdClass();

      foreach ($themeVals[$x] as $key => $v) {
        if ($key != 'Name') $returnObj->{$key} = $v;
      }

      return $returnObj; // return an object

    } elseif (
      gettype($themeVals[$x]) == 'object' &&
      isset($themeVals[$x]->Name) && $themeVals[$x]->Name == $theme
    ) {
      // die('object');
      $returnObj = array();

      foreach ($themeVals[$x] as $key => $v) {
        if ($key != 'Name') $returnObj[$key] = $v; // return an array
      }

      return $returnObj;
    }
  }
  return false;
}

function applyThemeToScssVars($theme, $fontSize, $fontWeight, $files) {
  $fileNum = 0;
  $returnVal = '';
  $scssString = '';
  $cssString = '';
  $cssVars = array();

  foreach ($files as $file) {

    if (!is_file($file)) die('file not found: '.$file);

    $handle = fopen($file, "r");
    $fileNum++;
    $lineNum = 0;
  
    if ($handle) {
      while (($line = fgets($handle)) !== false) {
        // process the line read.
        $lineNum++;

        $vars = explode(":", $line);
        if (strstr($file, '.inc.scss') && count($vars) == 2) {
          $cssVars[
            str_replace("$", "", trim($vars[0]))
           ] = str_replace(";", "", trim($vars[1]));

        } elseif(!strstr($line, '.inc.scss')) {
          $scssString .= $line;
        }
      }
      fclose($handle);
    } else {
      // error opening the file.
      die('Error: theme file could not be opened.');
    }
  }

  $updatedVals = array();
  $scssVarString = '';
  $scssVarsWithVariablesString = '';

  foreach ($theme as $t=>$v) {
    foreach ($cssVars as $c=>$v2) {
      if ($t == $c) {
        // echo "<br>set $t = $v";
        $updatedVals[$t] = $v;

        if (strstr($v, "$")) $scssVarsWithVariablesString .= "$".$t.": ".$v.";\n"; // Has a variable, save these for last
        else $scssVarString .= "$".$t.": ".$v.";\n";
      }
    }
  }
  if (!stristr($scssVarString, "fontSize")) {
    $scssVarString .= '$fontSize: '.$fontSize."px;\n";
  }
  if (!stristr($scssVarString, "fontWeight")) {
    $scssVarString .= '$fontWeight: '.$fontWeight.";\n";
  }
  // die('<pre>'.$scssVarString.'</pre>');
  $scssString = $scssVarString.$scssVarsWithVariablesString.$scssString;
  // die('<pre>'.$scssString.'</pre>');
  return $scssString;
}
?>
