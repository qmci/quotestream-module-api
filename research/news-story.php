<?php
$storyId = isset($_REQUEST['storyId']) ? htmlspecialchars($_REQUEST['storyId']) : '';
$qmEnv = isset($_REQUEST['qmEnv']) ? htmlspecialchars($_REQUEST['qmEnv']) : 'app';    
$qmLoader = isset($_REQUEST['qmLoader']) ? htmlspecialchars($_REQUEST['qmLoader']) : '';
$passEnv = ($qmEnv !== 'app') ? '&qmEnv=' . $qmEnv : '';
$passLoader = ($qmLoader !== '') ? '&qmLoader=' . $qmLoader : '';
$qmLoader = ($qmLoader !== '') ? $qmLoader . '.' : '';    
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Quotestream Trader - Research Module</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="//static.c1.quotemedia.com/qs/img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/qmod_custom.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/4.2.1/iframeResizer.contentWindow.min.js" integrity="sha256-pcBCSNXBj1IVeOk2N+lpQd0JBrC3E0u9BXNKG33l5ZA=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>

  <body>
    <div class="qm-tool-wrap">
      <div data-qmod-tool="fullnewsstory" data-qmod-params='{"storyId":"<?php echo $storyId; ?>"}' class="qtool"></div>
    </div>

    <script id="qmod" type="application/javascript" src="//<?php echo $qmLoader; ?>qmod.quotemedia.com/js/qmodLoader.js" data-qmod-env="<?php echo $qmEnv; ?>" data-qmod-wmid="102877"></script>
  </body>
</html>